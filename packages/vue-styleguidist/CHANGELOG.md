# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

### [5.0.2](https://github.com/vue-styleguidist/vue-styleguidist/compare/@mufg-mui/package@5.0.1...@mufg-mui/package@5.0.2) (2022-02-17)


### Bug Fixes

* Add debug output ([dc4f9c6](https://github.com/vue-styleguidist/vue-styleguidist/commit/dc4f9c60dfd71f10a92010fcadb875f6c1cce9b7))

### [5.0.1](https://github.com/vue-styleguidist/vue-styleguidist/compare/@mufg-mui/package@5.0.0...@mufg-mui/package@5.0.1) (2022-02-17)


### Bug Fixes

* Add mufg banner before build to make sure right styleguidist version is installed ([4e47528](https://github.com/vue-styleguidist/vue-styleguidist/commit/4e47528a6faa33df237f2382cb0e0b7e38059e01))

## 5.0.0 (2022-02-17)


### ⚠ BREAKING CHANGES

* **docgen:** props, events, methods and slots are now all arrays

Co-authored-by: Sébastien D. <demsking@gmail.com>
* **docgen:** required for props is never a string anymore
* **docgen:** docgen becomes async, so do all of the handlers
* change defaults for `simpleEditor` mean that `editorConfig` will not work without `simpleEditor: false`
* compiler now exports compiler function as default
* isCodeVueSfc, styleScoper and adaptCreateElement
 are now their own package
* Events that are not commented near their call
will not anymore be parsed. If you want to document events,
do it as a leading comment before your event call
* Webpack 3 support dropped

### Features

* 🎸 Add support for html language in examples ([77e225a](https://github.com/vue-styleguidist/vue-styleguidist/commit/77e225a3afcda1dabe87d7e52042748e48799d6b))
* add codeSplit option for compiler ([286e2ee](https://github.com/vue-styleguidist/vue-styleguidist/commit/286e2eee5a1af0b4e9ab948ea23d189a16391363))
* add copyCodeButton option ([90767af](https://github.com/vue-styleguidist/vue-styleguidist/commit/90767afdf8bc79b7c62434a4ffd79a0ab19aa94e))
* Add custom properties to the JSdoc ([54e69d4](https://github.com/vue-styleguidist/vue-styleguidist/commit/54e69d4c2df043a05389455f2c85fc27042d5236))
* add defaultExamples config ([52d7d90](https://github.com/vue-styleguidist/vue-styleguidist/commit/52d7d90b904a89c4ee73b3967dd1abb99099a1b0))
* add displayOrigins to vue-cli-ui ([df871db](https://github.com/vue-styleguidist/vue-styleguidist/commit/df871dba383b2c01dd6b021d36b62026b70ec447))
* add Higher order funciton to Compile pragmas ([5783eb4](https://github.com/vue-styleguidist/vue-styleguidist/commit/5783eb49406a695d0365728a7ea4dba2aed9b4fa))
* Add microfrontend lifecycle hooks and change build to umd ([4a2e8b7](https://github.com/vue-styleguidist/vue-styleguidist/commit/4a2e8b715ef561eee9d0c3eae3aa0ea82744cbc4))
* add minimize options ([93ad5d3](https://github.com/vue-styleguidist/vue-styleguidist/commit/93ad5d39a2a269b5bef50152c889c0418b74cb40))
* add option to disable progress bar ([6ec4e9d](https://github.com/vue-styleguidist/vue-styleguidist/commit/6ec4e9d80a0c061f3f287b82f2845ee6e490daee))
* add plugin for docgen cli ([a545aa5](https://github.com/vue-styleguidist/vue-styleguidist/commit/a545aa53d845bbacd5c372a0378a89b106feb41c)), closes [#614](https://github.com/vue-styleguidist/vue-styleguidist/issues/614)
* add progress bar while compiling ([f16b901](https://github.com/vue-styleguidist/vue-styleguidist/commit/f16b901842d795c3181ad57426f07bcc860d2957))
* add sponsor button ([59c7731](https://github.com/vue-styleguidist/vue-styleguidist/commit/59c773189e8fdacee33e2e75df1229c3df79a4d5))
* add tags to slots ([dcbddf8](https://github.com/vue-styleguidist/vue-styleguidist/commit/dcbddf82631d53422d5666ca1eb1971d828b4f04))
* add vue docgen simple laoder ([6d7e8f4](https://github.com/vue-styleguidist/vue-styleguidist/commit/6d7e8f49ed446fbb656616ebf87172fba459fb4a))
* Added mini-css-extract-plugin ([1ccbc4d](https://github.com/vue-styleguidist/vue-styleguidist/commit/1ccbc4d4b40cb71a24cef9d9ea8eed62d6ba3d02))
* Added new script in package.json to release a new version ([a7c26d3](https://github.com/vue-styleguidist/vue-styleguidist/commit/a7c26d39fb432c0a698a841c42069c3ed4a95cbf))
* Added sass-loader node-sass for customized example ([cc3c9fb](https://github.com/vue-styleguidist/vue-styleguidist/commit/cc3c9fbe4ab1da0b952d15fa13e62c588b28c30c))
* allow change padding of prism based editor ([d09b546](https://github.com/vue-styleguidist/vue-styleguidist/commit/d09b5466b3a230906da6d3c77ea177c1797abe72))
* allow compiler to render/compile JSX ([5084a39](https://github.com/vue-styleguidist/vue-styleguidist/commit/5084a39a92935c161d88bda56e6b929e7ee85fc8))
* allow components to be registered only locally ([#398](https://github.com/vue-styleguidist/vue-styleguidist/issues/398)) ([1dd2f1d](https://github.com/vue-styleguidist/vue-styleguidist/commit/1dd2f1d2820d496e109023cf96d0c837c11806d9)), closes [#2](https://github.com/vue-styleguidist/vue-styleguidist/issues/2)
* allow extension of component in a local file ([1663977](https://github.com/vue-styleguidist/vue-styleguidist/commit/166397793959e268d1ac7050c9aa49255de2c0a3))
* allow for config to be an async function ([fb16a67](https://github.com/vue-styleguidist/vue-styleguidist/commit/fb16a678770162dcb37de5400259c2935566e5fb))
* allow iev as valid component ([21c4874](https://github.com/vue-styleguidist/vue-styleguidist/commit/21c48740823c3fd790abd689985f0558afbb374b)), closes [#713](https://github.com/vue-styleguidist/vue-styleguidist/issues/713)
* allow import syntax ([5c61678](https://github.com/vue-styleguidist/vue-styleguidist/commit/5c6167843f002eca8dc46ef3b8cf19927abd66ef)), closes [#104](https://github.com/vue-styleguidist/vue-styleguidist/issues/104)
* allow mutiple extra example files ([d06283b](https://github.com/vue-styleguidist/vue-styleguidist/commit/d06283b841bca17db6125deb83bffc3ec565ac35))
* allow to ignore some example file lookup ([7104271](https://github.com/vue-styleguidist/vue-styleguidist/commit/71042712b28afc519ad631f873d8e62a87b821ae))
* allow to use the single type as Prop argument ([8e2a12d](https://github.com/vue-styleguidist/vue-styleguidist/commit/8e2a12d37a36a067b578833e76834b44c4a7029e)), closes [#1034](https://github.com/vue-styleguidist/vue-styleguidist/issues/1034)
* allow usage of prism themes ([921dbd5](https://github.com/vue-styleguidist/vue-styleguidist/commit/921dbd5e26c420d692f607a7f18bcff4e626d404))
* avoid skipping comps documented inline ([6ee0dff](https://github.com/vue-styleguidist/vue-styleguidist/commit/6ee0dff96cb3b7d07182fbd7fab075f664a42f28))
* change defaults for codeSplit & simpleEditor ([810bf1c](https://github.com/vue-styleguidist/vue-styleguidist/commit/810bf1ca511362c981c8c64cdcb8c1f31f2b74ae))
* **ci:** add caching to travis ([e4bf61b](https://github.com/vue-styleguidist/vue-styleguidist/commit/e4bf61b3942d924a8ca42921d167f732e88b8ae4))
* **ci:** Automated Semantic release ([#228](https://github.com/vue-styleguidist/vue-styleguidist/issues/228)) ([5c80183](https://github.com/vue-styleguidist/vue-styleguidist/commit/5c80183618acbb3631cbc283c3363e30d92e28ab))
* **cli:** add edit on github button for readme ([298bb68](https://github.com/vue-styleguidist/vue-styleguidist/commit/298bb68cfd3937729a13c06c61a8dff1a5d88c63))
* **cli:** allow for case insensitive readme ([80e0d8b](https://github.com/vue-styleguidist/vue-styleguidist/commit/80e0d8b51bc8f8c23a385438d6d626a753bb27fd))
* **cli:** expose docgen-cli config interfaces ([25f0744](https://github.com/vue-styleguidist/vue-styleguidist/commit/25f074491e7cd50f8f342e3f19e49c53f001d7d2))
* **cli:** render [@requires](https://github.com/requires) & [@example](https://github.com/example) ([94a2537](https://github.com/vue-styleguidist/vue-styleguidist/commit/94a25375c59c3bf94436aa28ece852ff66f3226c)), closes [#901](https://github.com/vue-styleguidist/vue-styleguidist/issues/901)
* **cli:** render props and bindings properly ([5e4c027](https://github.com/vue-styleguidist/vue-styleguidist/commit/5e4c0272e1f5b12a7223ad52055f68871525e27f)), closes [#1013](https://github.com/vue-styleguidist/vue-styleguidist/issues/1013)
* **cli:** render tags in a props description ([72af302](https://github.com/vue-styleguidist/vue-styleguidist/commit/72af30256b4b069c7d7d5120b6f600f970313d84)), closes [#1045](https://github.com/vue-styleguidist/vue-styleguidist/issues/1045)
* **cli:** resolve prettier config for MD gen ([0c04a73](https://github.com/vue-styleguidist/vue-styleguidist/commit/0c04a73dbe694ddc925661826fea54172242e49d))
* **cli:** update handling of subcomponents ([7d6f0ae](https://github.com/vue-styleguidist/vue-styleguidist/commit/7d6f0ae9c158b1ea39a70edbc499270f9a0b6605))
* **cli:** use writeStream for better performance ([25da08c](https://github.com/vue-styleguidist/vue-styleguidist/commit/25da08c60ad0748176b40a8db382263a77ac2e4b))
* collapsible sections ([43715a7](https://github.com/vue-styleguidist/vue-styleguidist/commit/43715a7532d07ba5f5f868a2628fbc10eae52543)), closes [#689](https://github.com/vue-styleguidist/vue-styleguidist/issues/689)
* **compiler:** styleScoper deals with deep ([ff89890](https://github.com/vue-styleguidist/vue-styleguidist/commit/ff89890b965056eed069ee1da5a7cccb29ab8662))
* **core:** update react styleguidist to 9.0.4 ([#344](https://github.com/vue-styleguidist/vue-styleguidist/issues/344)) ([1ec6e64](https://github.com/vue-styleguidist/vue-styleguidist/commit/1ec6e648f114067317ba0ab35fe499d6fb647dff))
* detect events in template ([327b54e](https://github.com/vue-styleguidist/vue-styleguidist/commit/327b54e5aa690aac039387cf6bb133d94c1774d9))
* detect model property ([1c28167](https://github.com/vue-styleguidist/vue-styleguidist/commit/1c28167c1ded9748c81f1e4784daf52005dd0f26)), closes [#654](https://github.com/vue-styleguidist/vue-styleguidist/issues/654)
* detect when example file loaded twice ([e4b1a48](https://github.com/vue-styleguidist/vue-styleguidist/commit/e4b1a4808f0b175bb0a23088e139595da58b14c4))
* display [@throws](https://github.com/throws) in the JsDoc tags of methods ([8325f86](https://github.com/vue-styleguidist/vue-styleguidist/commit/8325f8698a9a68a93db4afc88f093b041a920c14)), closes [#795](https://github.com/vue-styleguidist/vue-styleguidist/issues/795)
* **docgen:** accept more tags for event params ([cc55f58](https://github.com/vue-styleguidist/vue-styleguidist/commit/cc55f58c2169376bf56fda63a6708e431bcfdbdf))
* **docgen:** accept pug options for the template ([c318521](https://github.com/vue-styleguidist/vue-styleguidist/commit/c318521b29e0389d38c5eb8a1fed0c69969f8747))
* **docgen:** add exportName to CompoentDoc ([9466105](https://github.com/vue-styleguidist/vue-styleguidist/commit/94661050f2e853b78e2d1e02a4ad2e028446e1de))
* **docgen:** add external proptypes parsing for docgen ([eaa4748](https://github.com/vue-styleguidist/vue-styleguidist/commit/eaa4748ee5f89bf5f55fa186bad7f869a9b3ced9)), closes [#465](https://github.com/vue-styleguidist/vue-styleguidist/issues/465)
* **docgen:** add jsx option to docgen ([0ce2a9e](https://github.com/vue-styleguidist/vue-styleguidist/commit/0ce2a9eb4f4d1c606a31effcc8d8b66172d403ed))
* **docgen:** add origin to documentation object ([31e2fe2](https://github.com/vue-styleguidist/vue-styleguidist/commit/31e2fe21447b5174ebf1f9b5813f9f11e6ef740a)), closes [#594](https://github.com/vue-styleguidist/vue-styleguidist/issues/594)
* **docgen:** add support for arrays in aliases ([df76397](https://github.com/vue-styleguidist/vue-styleguidist/commit/df763973b03d7d8977f25d6eafae1a6faa0507b4))
* **docgen:** add support for Identifier when parsing validators ([#1076](https://github.com/vue-styleguidist/vue-styleguidist/issues/1076)) ([5f0d089](https://github.com/vue-styleguidist/vue-styleguidist/commit/5f0d089bd4159f5098148df67db5339e473da6d9))
* **docgen:** add vuetify exported component ([932e2ec](https://github.com/vue-styleguidist/vue-styleguidist/commit/932e2ec6e51402db365b6de15f36762bf999184e))
* **docgen:** allow destructured children in func ([1f9d9b6](https://github.com/vue-styleguidist/vue-styleguidist/commit/1f9d9b6df6f81cbe56aa31b3fa3fd50ff9dd858c))
* **docgen:** allow other forms of validation ([dd2400c](https://github.com/vue-styleguidist/vue-styleguidist/commit/dd2400cbaecad10209625c2046674d037e387d93))
* **docgen:** allow slots to be defined by composition API render functions ([63f2f35](https://github.com/vue-styleguidist/vue-styleguidist/commit/63f2f352435f95fc55e3598c877c33383909e933))
* **docgen:** allow to customize validExtends ([eb966c5](https://github.com/vue-styleguidist/vue-styleguidist/commit/eb966c51a6e62fdb2e1121942f7676dff02ae9e5))
* **docgen:** allow to force events definitions ([#854](https://github.com/vue-styleguidist/vue-styleguidist/issues/854)) ([7a2105c](https://github.com/vue-styleguidist/vue-styleguidist/commit/7a2105c84aa9b08d1b380ea56b698aaedad1e9e8))
* **docgen:** allow to have more than 1 values tag ([3e84005](https://github.com/vue-styleguidist/vue-styleguidist/commit/3e840058615aea163e5ca5e8f3ab1ec9324ffd4a))
* **docgen:** allow typescript as in exports ([0514a86](https://github.com/vue-styleguidist/vue-styleguidist/commit/0514a86ead9cc4ddb9b17b5a0857c8185d70af7a)), closes [#1066](https://github.com/vue-styleguidist/vue-styleguidist/issues/1066)
* **docgen:** allow wrap export in if ([5744801](https://github.com/vue-styleguidist/vue-styleguidist/commit/574480190cdf9cd4480b7f3615df7925975206ef))
* **docgen:** class event [@emit](https://github.com/emit) ([4483168](https://github.com/vue-styleguidist/vue-styleguidist/commit/4483168d42b8f4e10a228e74002760153f18fafc)), closes [#305](https://github.com/vue-styleguidist/vue-styleguidist/issues/305)
* **docgen:** deal with index as filename ([61d28f5](https://github.com/vue-styleguidist/vue-styleguidist/commit/61d28f5ba60b36a8d8af55c4366497b369d940e5))
* **docgen:** defilter custom doclet tag titles ([f8c7bbf](https://github.com/vue-styleguidist/vue-styleguidist/commit/f8c7bbf19921d7d7cf4bf4057ac56301a3b21183))
* **docgen:** detect values in validator ([8d681a6](https://github.com/vue-styleguidist/vue-styleguidist/commit/8d681a66576990f2acdc265cea7f4ffa4659b14e))
* **docgen:** expose docs block in dogen-api ([4565559](https://github.com/vue-styleguidist/vue-styleguidist/commit/4565559776fc8f8355ffc9b401188ea77258fc99))
* **docgen:** extract type values properly ([6ffd571](https://github.com/vue-styleguidist/vue-styleguidist/commit/6ffd571fe1d51048a413eb0908d873da916d3dda))
* **docgen:** make props detector work [WIP] ([886d222](https://github.com/vue-styleguidist/vue-styleguidist/commit/886d2223c960ef329fddcb076821b5d4dc9aeb81))
* **docgen:** make values work for class type ([c003176](https://github.com/vue-styleguidist/vue-styleguidist/commit/c003176186616975afa72083efb22a1e4f45eb8a))
* **docgen:** methods returned by other methods ([95e648c](https://github.com/vue-styleguidist/vue-styleguidist/commit/95e648c784773b57603e0f8ebca652a5f3a76b5d)), closes [#765](https://github.com/vue-styleguidist/vue-styleguidist/issues/765)
* **docgen:** multi-components in a file ([3790837](https://github.com/vue-styleguidist/vue-styleguidist/commit/3790837054e38cf43510e095124ebb5b15645f5e))
* **docgen:** parse emits option from vue 3 ([0469224](https://github.com/vue-styleguidist/vue-styleguidist/commit/0469224f92355dfa867a528f8123d7ef181a387c)), closes [#965](https://github.com/vue-styleguidist/vue-styleguidist/issues/965)
* **docgen:** parse the throws tag ([2f70045](https://github.com/vue-styleguidist/vue-styleguidist/commit/2f7004551ea71a2a469053ccb348f3bea21ac867))
* **docgen:** record [@type](https://github.com/type) values ([452ccb5](https://github.com/vue-styleguidist/vue-styleguidist/commit/452ccb5c3a708a047183a1684201c17487cf10ec))
* **docgen:** refactor bindings ([b501f82](https://github.com/vue-styleguidist/vue-styleguidist/commit/b501f82f9461699ca050f92087af57260e79fc18))
* **docgen:** remove @Component constraint on class component ([ffe7725](https://github.com/vue-styleguidist/vue-styleguidist/commit/ffe7725fc21a1afe2f094b404536f74d8c4c65dd)), closes [#313](https://github.com/vue-styleguidist/vue-styleguidist/issues/313)
* **docgen:** remove @Component constraint on class component ([f3f00d8](https://github.com/vue-styleguidist/vue-styleguidist/commit/f3f00d8a7e4c18084e83a29c1891e9543dc6b178)), closes [#313](https://github.com/vue-styleguidist/vue-styleguidist/issues/313)
* **docgen:** resolve dynamic mixins ([0dbe049](https://github.com/vue-styleguidist/vue-styleguidist/commit/0dbe0493e786b49903d0cef0255df531713186fa))
* **docgen:** resolve pass through components ([07d183f](https://github.com/vue-styleguidist/vue-styleguidist/commit/07d183faad4bb2125bb389dcc065865d2d105dcb))
* **docgen:** resolve values in as types ([7648a4e](https://github.com/vue-styleguidist/vue-styleguidist/commit/7648a4e199314af25cc25183f1fd2f506da447fa))
* **docgen:** support ts prop types ([c57c243](https://github.com/vue-styleguidist/vue-styleguidist/commit/c57c243ae31d20843532d13fba830443f3e22732)), closes [#413](https://github.com/vue-styleguidist/vue-styleguidist/issues/413)
* **docgen:** undetecteable slots definition ([be867bd](https://github.com/vue-styleguidist/vue-styleguidist/commit/be867bda8270a47a197c0f04f47d1a35425feace))
* Document composite components ([#815](https://github.com/vue-styleguidist/vue-styleguidist/issues/815)) ([a6a3d11](https://github.com/vue-styleguidist/vue-styleguidist/commit/a6a3d11c320a3501ea1e63acdf3108d191cc6390)), closes [#809](https://github.com/vue-styleguidist/vue-styleguidist/issues/809)
* emit types for vue-styleguidist ([f0af958](https://github.com/vue-styleguidist/vue-styleguidist/commit/f0af95864b6665c788c85196af794f6e9d66c24a))
* enable indpdt HMR on sub-components ([3626933](https://github.com/vue-styleguidist/vue-styleguidist/commit/36269332f42ad3a497e763887f929dec0df24c8d))
* export config types to help configure ([0b44fc6](https://github.com/vue-styleguidist/vue-styleguidist/commit/0b44fc61bf6113aeb33a9b520cb3458df66b93f5))
* expose typescript types for theming ([3110fb5](https://github.com/vue-styleguidist/vue-styleguidist/commit/3110fb5b8342b3c89a70e9ecaf710a4c3a77bee5))
* extract footer from styleguidist for custom ([907271f](https://github.com/vue-styleguidist/vue-styleguidist/commit/907271f6f6beaf0f70c7f51ac1fe070731e74551)), closes [#935](https://github.com/vue-styleguidist/vue-styleguidist/issues/935)
* figure out the move to vue 3 [WIP] ([30ab312](https://github.com/vue-styleguidist/vue-styleguidist/commit/30ab31228345e6d43062740f7c9af0222457472f))
* give default examples a variable geometry ([535e347](https://github.com/vue-styleguidist/vue-styleguidist/commit/535e347e3970b5c48a40ac538892cffe85a89977))
* **main:** add jsxInComponents option ([27b4257](https://github.com/vue-styleguidist/vue-styleguidist/commit/27b4257db0e2277a01361b11b1bbb816cf6f78d6))
* make arrow functions default cleaner ([f16b424](https://github.com/vue-styleguidist/vue-styleguidist/commit/f16b42403797ac78ad168c054134cbd2f7cccc97))
* origin column on props event methods & slots ([8b0650f](https://github.com/vue-styleguidist/vue-styleguidist/commit/8b0650f08d3c4cde0970fd87aabb439cd1e06ef0))
* page per section and component, and fixes bugs of navigation ([ed2e373](https://github.com/vue-styleguidist/vue-styleguidist/commit/ed2e373273dc0c21530c1f42eb45d3238f4e2b9a))
* parse $emit in templates ([21d5eca](https://github.com/vue-styleguidist/vue-styleguidist/commit/21d5ecafea66a1cdc1eb58387fd19bb9cb394437)), closes [#725](https://github.com/vue-styleguidist/vue-styleguidist/issues/725)
* pass validExtends to styleguide.config.js ([c22f7d5](https://github.com/vue-styleguidist/vue-styleguidist/commit/c22f7d599c8d1783791bd745934f301a3d7e0489))
* **plugin/ui:** add first version of @vue/ui support ([150215a](https://github.com/vue-styleguidist/vue-styleguidist/commit/150215a98c95d78036b9c2680dcd330f0d299d6f)), closes [#306](https://github.com/vue-styleguidist/vue-styleguidist/issues/306)
* **plugin:** better default config for ([9a19cc4](https://github.com/vue-styleguidist/vue-styleguidist/commit/9a19cc452bffeb325479dcf041d7a1c18807d2d7))
* **plugin:** reference jsxInComponents in vueui ([a9646ef](https://github.com/vue-styleguidist/vue-styleguidist/commit/a9646ef580b6baece89b93f24b64f0d6c8f106f6))
* readable css class for JsDoc results ([a56f341](https://github.com/vue-styleguidist/vue-styleguidist/commit/a56f341921978cb459662bea7cc397e9bfb36cef)), closes [#602](https://github.com/vue-styleguidist/vue-styleguidist/issues/602)
* Removed sass-loader ([6bfea25](https://github.com/vue-styleguidist/vue-styleguidist/commit/6bfea2531154270cbacb3ad8f8b1fccac839d24a))
* render complex types properly ([a756455](https://github.com/vue-styleguidist/vue-styleguidist/commit/a756455bf17e751d2b4f6179e79f3fc6a2920a97))
* resolve local global variable for mixins ([58305f3](https://github.com/vue-styleguidist/vue-styleguidist/commit/58305f3df5722dee20a06da534f02d481283f505))
* review design of all props output ([cc80bd5](https://github.com/vue-styleguidist/vue-styleguidist/commit/cc80bd58d598ab94082f9dac877cf927cbb94469))
* review the style of default functions ([98ae04c](https://github.com/vue-styleguidist/vue-styleguidist/commit/98ae04cc17781e57f610a8a770eb8919bae2a66a))
* support jsx components ([#314](https://github.com/vue-styleguidist/vue-styleguidist/issues/314)) ([562ac9d](https://github.com/vue-styleguidist/vue-styleguidist/commit/562ac9d909445fa9c5bab101c232362ff792914f)), closes [#292](https://github.com/vue-styleguidist/vue-styleguidist/issues/292)
* support jsx components ([#314](https://github.com/vue-styleguidist/vue-styleguidist/issues/314)) ([398a868](https://github.com/vue-styleguidist/vue-styleguidist/commit/398a868d7f51c447fb516bd36e65636eecb5190e)), closes [#292](https://github.com/vue-styleguidist/vue-styleguidist/issues/292)
* transfrom styleguidist into a monorepo ([#293](https://github.com/vue-styleguidist/vue-styleguidist/issues/293)) ([c6fa190](https://github.com/vue-styleguidist/vue-styleguidist/commit/c6fa1907524ecc7223a5dae0bf93fb5caca75c57))
* update docgen api to 3.0 ([e2f9b48](https://github.com/vue-styleguidist/vue-styleguidist/commit/e2f9b487ccaeae641dcfdc254f38f9fc8a1bc830))
* update react-styleguidist to 8.0 ([44fa196](https://github.com/vue-styleguidist/vue-styleguidist/commit/44fa19607b0bc36f80572c039caa6289ab3a52c0)), closes [#229](https://github.com/vue-styleguidist/vue-styleguidist/issues/229)
* update rsg with new theming ([af0ceb2](https://github.com/vue-styleguidist/vue-styleguidist/commit/af0ceb2bba6f9b8fe6a000c121e02b7a2e435c8c))
* Updated dependencies ([b28e17f](https://github.com/vue-styleguidist/vue-styleguidist/commit/b28e17f64f89855cb44475532ea212655b19ea47))
* Updated dependencies in example ([ab90c67](https://github.com/vue-styleguidist/vue-styleguidist/commit/ab90c677485cb991f58040b539f52e9e8e896e9d))
* Updated examples [#151](https://github.com/vue-styleguidist/vue-styleguidist/issues/151) ([b2d0d5b](https://github.com/vue-styleguidist/vue-styleguidist/commit/b2d0d5bbace2f5f34a9d04b6b6d8e8e154e417e7))
* Updated file release.js ([3e7845c](https://github.com/vue-styleguidist/vue-styleguidist/commit/3e7845c191e7ddeae4dd41c849ec9e02d9cd4c27))
* Updated package-lock ([44f7970](https://github.com/vue-styleguidist/vue-styleguidist/commit/44f7970ac6e08770f7b3d366420e3f914c6b315d))
* Updated README ([c48b13e](https://github.com/vue-styleguidist/vue-styleguidist/commit/c48b13e995dcd294fb8606e990078c5193848db6))
* Updated vue-docgen-api ([49749f3](https://github.com/vue-styleguidist/vue-styleguidist/commit/49749f3ea58f939b84d05e456f0523b2324becbf))
* Updated vuecli3 example ([b05acd2](https://github.com/vue-styleguidist/vue-styleguidist/commit/b05acd2c73749830b853fa47478a87cddb402330))
* Updated with styleguidist ([#175](https://github.com/vue-styleguidist/vue-styleguidist/issues/175)) ([3ce32b1](https://github.com/vue-styleguidist/vue-styleguidist/commit/3ce32b1c99b7e9e37cce63811a47b769d2237b59))
* use [@values](https://github.com/values) tag in props ([cb2fc74](https://github.com/vue-styleguidist/vue-styleguidist/commit/cb2fc743aef7c9c7e6ac710f739277e9eda8637e)), closes [#345](https://github.com/vue-styleguidist/vue-styleguidist/issues/345)
* use bindings comments in styleguidist ([4fb6551](https://github.com/vue-styleguidist/vue-styleguidist/commit/4fb655141ecd3018d837f3e63a6b8de3b5263f53))
* use pug-loader options to fuel docgen ([d2103fe](https://github.com/vue-styleguidist/vue-styleguidist/commit/d2103febadf74722945642dfefc0f4f0cdf493ac))
* use styleguidePublicPath in server ([bd5e3ec](https://github.com/vue-styleguidist/vue-styleguidist/commit/bd5e3ecc065b15369a8a6df2cc579772b329320d))
* use the functional tag in docgen cli ([c6f8725](https://github.com/vue-styleguidist/vue-styleguidist/commit/c6f8725b5d8afa430d786627690393f89c51d541))
* use the JSX capabilities of compiler ([a6db6cb](https://github.com/vue-styleguidist/vue-styleguidist/commit/a6db6cbafcbb8ce7614257a57345f9d96ac073fe))
* **utils:** add selector combinators support for styleScoper ([283f805](https://github.com/vue-styleguidist/vue-styleguidist/commit/283f805c12a9b24358ed9cfe9ff36a349fd4e00e))
* **utils:** add the ability to remove the added scoped style ([51baa00](https://github.com/vue-styleguidist/vue-styleguidist/commit/51baa00b0cb0425e51f61bd413ff5868313e8614))
* vue-docgen CLI to generate markdown files ([b05d7d3](https://github.com/vue-styleguidist/vue-styleguidist/commit/b05d7d3662432564af82aa1275edada4bcfb9405))
* vue-docgen-cli if getDestFile should not create md file ([55da63e](https://github.com/vue-styleguidist/vue-styleguidist/commit/55da63e3a5452fe4bda9a5ba6ee04d465cf48c29))
* when codeSplit lazy load codemirror editor ([6f83989](https://github.com/vue-styleguidist/vue-styleguidist/commit/6f83989f90c0c0f25e878054d47b525d8aa191ee))


### Bug Fixes

*  evaluation was failing ([467949f](https://github.com/vue-styleguidist/vue-styleguidist/commit/467949f48b4388a50c4919954d8c081beb7ee871))
* [#120](https://github.com/vue-styleguidist/vue-styleguidist/issues/120) ([1a4eb82](https://github.com/vue-styleguidist/vue-styleguidist/commit/1a4eb82feab651a1648a3c85cbe4ef32cfc62001))
* [#96](https://github.com/vue-styleguidist/vue-styleguidist/issues/96) ([3869e10](https://github.com/vue-styleguidist/vue-styleguidist/commit/3869e10a073736472654f930346177723d32600c))
* <docs src=> should not look at script tag ([2cef0d4](https://github.com/vue-styleguidist/vue-styleguidist/commit/2cef0d42b5060905e576c30dce375d100d9ef260))
* accept multiple style parts ([9a6b031](https://github.com/vue-styleguidist/vue-styleguidist/commit/9a6b0318e3a2163ed86185c8d7cacfda90acd69a))
* adapt style of sub-component ([a58f99a](https://github.com/vue-styleguidist/vue-styleguidist/commit/a58f99a4942301137b0601bb39b490243e765455))
* add .gitattributes to avoid crlf issues ([92ded66](https://github.com/vue-styleguidist/vue-styleguidist/commit/92ded663e7b72f1dc0f870b2f1f72d6dc0cfc3c9)), closes [#202](https://github.com/vue-styleguidist/vue-styleguidist/issues/202)
* add .vue to extension array in webpack config ([65da41b](https://github.com/vue-styleguidist/vue-styleguidist/commit/65da41b1506a3fd36ca6731bb066a2574dde1480))
* add js changes in config ([bf9880d](https://github.com/vue-styleguidist/vue-styleguidist/commit/bf9880da4292e171574a075feb838728a528b702))
* add simple bindings detection ([31a3fca](https://github.com/vue-styleguidist/vue-styleguidist/commit/31a3fcaf86a67cd87e2b8145e57b89ca29da5bfc))
* add vue-class-component management ([#371](https://github.com/vue-styleguidist/vue-styleguidist/issues/371)) ([d1aced1](https://github.com/vue-styleguidist/vue-styleguidist/commit/d1aced1af52678dfa7fc39fd02a52626f95eaef9))
* add warning when using editorConfig ([b39f6f8](https://github.com/vue-styleguidist/vue-styleguidist/commit/b39f6f851455f208a9ae9092c8226cf2f7c3322c))
* add webpack peerDependency ([16b1fa7](https://github.com/vue-styleguidist/vue-styleguidist/commit/16b1fa7e97b2215c2426080a128a10c0db2b7393))
* Added vue-webpack-loaders [#181](https://github.com/vue-styleguidist/vue-styleguidist/issues/181) ([07a0a8b](https://github.com/vue-styleguidist/vue-styleguidist/commit/07a0a8ba4f0c76bd36a9740765be64e333f5cc71))
* Additionally try absolute require.resolve in resolvePathFrom ([d1be583](https://github.com/vue-styleguidist/vue-styleguidist/commit/d1be5831ed69ef53552a4c98c6f5659e3f0afa4a))
* adjust structure of examples ([b80a86c](https://github.com/vue-styleguidist/vue-styleguidist/commit/b80a86c3aad769530cdab4ea911639088a2b8dac))
* allow default example with custom format ([7ac7f57](https://github.com/vue-styleguidist/vue-styleguidist/commit/7ac7f57f0359be7c2b19017c6a8a4a1616c3cfd8))
* allow examples start with < & pure template ([3860129](https://github.com/vue-styleguidist/vue-styleguidist/commit/3860129c4c7bac644df39e6ac3a128a4e09ea84d))
* allow externals in webpack config ([#256](https://github.com/vue-styleguidist/vue-styleguidist/issues/256)) ([3fdea7a](https://github.com/vue-styleguidist/vue-styleguidist/commit/3fdea7acaed996bc3b2676e54a8b3f451779fd79))
* allow for @Emit to be parsed ([b1c7285](https://github.com/vue-styleguidist/vue-styleguidist/commit/b1c7285943de709a5aa4c518c7570bf185324aa9))
* allow for new Vue in jsx ([45c62c9](https://github.com/vue-styleguidist/vue-styleguidist/commit/45c62c9473eab5bced7edbdbc355a6136619565c))
* allow function expression validators ([7ece101](https://github.com/vue-styleguidist/vue-styleguidist/commit/7ece101eabcfdfe4003b18587a21e05825d316eb)), closes [#1083](https://github.com/vue-styleguidist/vue-styleguidist/issues/1083)
* allow ignoreWithoutExamples to work with docs ([f5bbc41](https://github.com/vue-styleguidist/vue-styleguidist/commit/f5bbc416e108db1b313d4a2c55c8e1bc845d03ca))
* allow importing non component files ([5aa59a6](https://github.com/vue-styleguidist/vue-styleguidist/commit/5aa59a666d6177da1de7b8e13d76e83cbd55ef95)), closes [#436](https://github.com/vue-styleguidist/vue-styleguidist/issues/436)
* allow vue code blocks without script parts in examples markdown ([#272](https://github.com/vue-styleguidist/vue-styleguidist/issues/272)) ([d30b61b](https://github.com/vue-styleguidist/vue-styleguidist/commit/d30b61bb6b9e35fca5d0514702d9ada4c28156ab)), closes [#269](https://github.com/vue-styleguidist/vue-styleguidist/issues/269)
* allow webpackConfig functions to return alias ([5a6b6a3](https://github.com/vue-styleguidist/vue-styleguidist/commit/5a6b6a30ba609b2ddfbf0b6d894e802ca157f724)), closes [#793](https://github.com/vue-styleguidist/vue-styleguidist/issues/793)
* an SFC example can contain JSX ([deb2dc7](https://github.com/vue-styleguidist/vue-styleguidist/commit/deb2dc7ebda80938d59ab458faa8699f7305eb35))
* **assets:** move logo back to root ([12a3821](https://github.com/vue-styleguidist/vue-styleguidist/commit/12a38215f345993dcff8924b6c2e18725907b59e))
* async conlict with routing ([75424f7](https://github.com/vue-styleguidist/vue-styleguidist/commit/75424f73b66e8b6b21d00fd32af461c19bb36560))
* avoid cors issue on codesandbox ([26450b2](https://github.com/vue-styleguidist/vue-styleguidist/commit/26450b28535eeb032385a99799a05f6ccf1f7951))
* avoid defaulting outDir if outFile ([8a2a282](https://github.com/vue-styleguidist/vue-styleguidist/commit/8a2a282e4dadda61ec41789ec8e4a80bf65e0a8e))
* avoid dependency to webpack ([63ee996](https://github.com/vue-styleguidist/vue-styleguidist/commit/63ee996e70a36307b62ac76e691e9b1bb1a9de36))
* avoid double progressBar ([e39878e](https://github.com/vue-styleguidist/vue-styleguidist/commit/e39878e903ecba1f8d8651d5a28abca53072f55c))
* avoid error when multiple return in a default ([3e4c53d](https://github.com/vue-styleguidist/vue-styleguidist/commit/3e4c53d8836c40d826c26de768e4b41e8d3b4205))
* avoid hmr loop in plugin usage ([c6e4adf](https://github.com/vue-styleguidist/vue-styleguidist/commit/c6e4adf9ec9e40c54e959879413f3b19d34dc5f8))
* avoid progress bar when verbose ([75f77d0](https://github.com/vue-styleguidist/vue-styleguidist/commit/75f77d0d6f92fa2f72ec9f625ec72c3c76077c92))
* avoid systematic verbose ([d43c6b0](https://github.com/vue-styleguidist/vue-styleguidist/commit/d43c6b0d8b6b5c782d806925eccd6d4b95526292))
* avoid the squiggles in main menu ([5cc8f93](https://github.com/vue-styleguidist/vue-styleguidist/commit/5cc8f931615e19d80b4031ee7ce27a4288e59ccf))
* babel typescript snafu ([d72c43e](https://github.com/vue-styleguidist/vue-styleguidist/commit/d72c43e4576ba666d60e32cdaab329c560022ef0)), closes [#639](https://github.com/vue-styleguidist/vue-styleguidist/issues/639)
* babel warning about uglify ([#255](https://github.com/vue-styleguidist/vue-styleguidist/issues/255)) ([ccf2bc8](https://github.com/vue-styleguidist/vue-styleguidist/commit/ccf2bc8e16298fb2db05685c0d15affc2146dc19))
* basic example using babel 7 ([b0632e4](https://github.com/vue-styleguidist/vue-styleguidist/commit/b0632e42a84fd69d05807d65821b507b6bc50914)), closes [#205](https://github.com/vue-styleguidist/vue-styleguidist/issues/205)
* better PropTypes for PlaygroundAsync ([3b60e3e](https://github.com/vue-styleguidist/vue-styleguidist/commit/3b60e3e7627718a6f9ac5617a166b8455dd11ec3))
* bring back last version of acorn ([1f7ee42](https://github.com/vue-styleguidist/vue-styleguidist/commit/1f7ee42eae899627788c5804cc36a2c6ff502452))
* bring back react-dev-utils hat ([9ba9a49](https://github.com/vue-styleguidist/vue-styleguidist/commit/9ba9a49acbec23ea124fd6e79ada51fb7f4b4c82))
* bring back the full power of verbose option ([210bae2](https://github.com/vue-styleguidist/vue-styleguidist/commit/210bae2c9e5b935c17cb7add5bfdef9459b90a6c))
* bump clipboard-copy version ([b3c86d9](https://github.com/vue-styleguidist/vue-styleguidist/commit/b3c86d95a221c79312b176f93ec6e6e91623025d)), closes [#500](https://github.com/vue-styleguidist/vue-styleguidist/issues/500)
* **cache:** LRUCache instantiation ([#250](https://github.com/vue-styleguidist/vue-styleguidist/issues/250)) ([ff6332f](https://github.com/vue-styleguidist/vue-styleguidist/commit/ff6332fcf5c7156cd9676449c2c06593f518faab)), closes [#249](https://github.com/vue-styleguidist/vue-styleguidist/issues/249)
* call to  ast-types builder ([071b067](https://github.com/vue-styleguidist/vue-styleguidist/commit/071b06717b9d93c125e71ec07e054a3a9811c58f))
* ci should use simlinks instead of copy ([14293d5](https://github.com/vue-styleguidist/vue-styleguidist/commit/14293d555d1b08b259b7db1c2611de52da0dcbf1))
* **ci:** add compilation before release stage ([e744161](https://github.com/vue-styleguidist/vue-styleguidist/commit/e74416105d38e9bebaecb12d921667f09be57b63))
* **ci:** again fix semantic release (load the version) ([991cfbd](https://github.com/vue-styleguidist/vue-styleguidist/commit/991cfbd307da9c97064a554bbfb13e2b9e80ed0c))
* **ci:** condition release and doc stage ([e955f67](https://github.com/vue-styleguidist/vue-styleguidist/commit/e955f672c6e42e00ff69e1593f905e4d7f608207))
* **ci:** reinstall node-fetch... ([2f4ea06](https://github.com/vue-styleguidist/vue-styleguidist/commit/2f4ea0677c0fea772370b9c4c23c612ef6dff684))
* **ci:** remove all mentions of webpack 3 ([8a04486](https://github.com/vue-styleguidist/vue-styleguidist/commit/8a0448645a4f66890ad48bc8db0219772c3f1804))
* **ci:** remove deploy from semantic release ([5920881](https://github.com/vue-styleguidist/vue-styleguidist/commit/5920881960f08cf6f8328d2fdfbb6b357bb4570e))
* **ci:** rolback it all ([03e67de](https://github.com/vue-styleguidist/vue-styleguidist/commit/03e67de81fd46430aab326939d1cab4820f86eb0))
* **ci:** split lerna in ci between version and publish ([952e5cd](https://github.com/vue-styleguidist/vue-styleguidist/commit/952e5cd3e9599e92e778425f8103477065e1bdb1))
* **ci:** stop artificially install node-fetch in release ([7f0dbd8](https://github.com/vue-styleguidist/vue-styleguidist/commit/7f0dbd80c86b31d8c485b6a461e31f5f235247b5))
* **ci:** try using version 15.12 of semantic-release ([64fced9](https://github.com/vue-styleguidist/vue-styleguidist/commit/64fced96286519176eb9e3e28fa81b392187fd8f))
* **ci:** update node-fetch to a version compatible with github ([6192864](https://github.com/vue-styleguidist/vue-styleguidist/commit/61928642de155177768fafb66dcd2ae653647f9c))
* classname and style were ignored ([563b313](https://github.com/vue-styleguidist/vue-styleguidist/commit/563b3134ee1304790d06b2f4d394cc79400be9da))
* cleanComponentName peace with babel 7 ([bd8a085](https://github.com/vue-styleguidist/vue-styleguidist/commit/bd8a08571f1b9d8b60052db0598fc03244e07a28))
* **cli:** allow partial templates ([62f00b6](https://github.com/vue-styleguidist/vue-styleguidist/commit/62f00b6eb7b1083d66723eb20858dd2447c09f67))
* **cli:** docgen-cli does not require prettierrc ([a12bc82](https://github.com/vue-styleguidist/vue-styleguidist/commit/a12bc822fcc342ac762de9102989adde02b8b8c9)), closes [#914](https://github.com/vue-styleguidist/vue-styleguidist/issues/914)
* **cli:** make achors calsulations work on win ([170205d](https://github.com/vue-styleguidist/vue-styleguidist/commit/170205d34dde1e95f111b2fab63e68f635a43b43))
* **cli:** refresh when updating markdown files ([2c3c502](https://github.com/vue-styleguidist/vue-styleguidist/commit/2c3c5026b5624693dad2c3f1e851a4e5e634971f)), closes [#885](https://github.com/vue-styleguidist/vue-styleguidist/issues/885)
* **cli:** replace all instances of sep on windows ([7a76b92](https://github.com/vue-styleguidist/vue-styleguidist/commit/7a76b920219ac1fb650738459c177c19cf04dfa6))
* **cli:** simplify bin for docgen ([73a5c58](https://github.com/vue-styleguidist/vue-styleguidist/commit/73a5c58229dd7ce9eb75895ad106132c8c2f186f))
* **cli:** take cwd param into account ([7935956](https://github.com/vue-styleguidist/vue-styleguidist/commit/7935956ea1f15ade807d8dd3b9a8321095e15b6b))
* **codemirror:** allow for mulitple words in cm themes ([6168883](https://github.com/vue-styleguidist/vue-styleguidist/commit/61688835e3b8b83c3aa3676908189f9f9f57939f)), closes [#480](https://github.com/vue-styleguidist/vue-styleguidist/issues/480)
* collapsible sections tocMode ([e5f7bfd](https://github.com/vue-styleguidist/vue-styleguidist/commit/e5f7bfdfc70acaa97ed7ae297363c418dfec3001))
* combining new Vue and imports was impossible ([d37359c](https://github.com/vue-styleguidist/vue-styleguidist/commit/d37359cb9bfa6c4576b49e27e3cbc8a4d5e261bf))
* compile issue linked to progress bar fix ([5c55eaf](https://github.com/vue-styleguidist/vue-styleguidist/commit/5c55eaf5927bbb3e9479a916f9c59cab6416a56c))
* **compiler:** add normal attributes in attrs ([be6de16](https://github.com/vue-styleguidist/vue-styleguidist/commit/be6de16485a159c9e93b80713ec2c5ca1fe32fce))
* **compiler:** avoid conflict, rollup buble+acorn ([8c6d23a](https://github.com/vue-styleguidist/vue-styleguidist/commit/8c6d23a635a2218b9053bcab2f0d9bf901b460d7))
* **compiler:** error location reporting ([34121b5](https://github.com/vue-styleguidist/vue-styleguidist/commit/34121b50a0da31d92b5d47fcd94e2b23634bffce))
* **compiler:** make sure files with the same name wont conflict ([98a1b76](https://github.com/vue-styleguidist/vue-styleguidist/commit/98a1b7662c3b58ad28f0751bc6a8f9c6c3cf661d)), closes [#471](https://github.com/vue-styleguidist/vue-styleguidist/issues/471)
* **compiler:** make the jsx spread work vue style ([27dd670](https://github.com/vue-styleguidist/vue-styleguidist/commit/27dd6702f1edf78c339410dfeb58faac801ecc6d))
* **compiler:** only allow max version of each bro ([a062a54](https://github.com/vue-styleguidist/vue-styleguidist/commit/a062a5405b9793e46bf33ec49a6f3d6511fb7e28))
* **compiler:** re-enable compilation in vue SFC ([5bb99c3](https://github.com/vue-styleguidist/vue-styleguidist/commit/5bb99c3b4760e1b753b57fbe7022bd67c3950d1c)), closes [#456](https://github.com/vue-styleguidist/vue-styleguidist/issues/456)
* **compiler:** types of buble transform ([6134866](https://github.com/vue-styleguidist/vue-styleguidist/commit/613486628511b2113fcda5e7377207ce70e00f23))
* **core:** example loader needs to require only on the script ([0c045df](https://github.com/vue-styleguidist/vue-styleguidist/commit/0c045df560d7a553ccd33b61f252a045a0580b4c)), closes [#421](https://github.com/vue-styleguidist/vue-styleguidist/issues/421)
* **core:** fix Preview.js with pure md files ([d52feea](https://github.com/vue-styleguidist/vue-styleguidist/commit/d52feea91f2f2ef93e35b2bd9a430f84df97e258)), closes [#411](https://github.com/vue-styleguidist/vue-styleguidist/issues/411)
* **core:** remove self require in readme ([b6408af](https://github.com/vue-styleguidist/vue-styleguidist/commit/b6408af668d71c54b5cd5d3bbe7dd5c03d9f8346)), closes [#407](https://github.com/vue-styleguidist/vue-styleguidist/issues/407)
* create-server bis for csb ([0768c51](https://github.com/vue-styleguidist/vue-styleguidist/commit/0768c5143d41762caf59f5d07a1e0bc74a2570e3))
* cypress tetss failing using previewAsync ([62c7716](https://github.com/vue-styleguidist/vue-styleguidist/commit/62c7716f2785feee2c63ba664cd0aaee5f707622))
* Default components pattern ([b7ffe5f](https://github.com/vue-styleguidist/vue-styleguidist/commit/b7ffe5f9e245275489e36ffde443b06ae4815d34))
* default editor font-family ([909a47f](https://github.com/vue-styleguidist/vue-styleguidist/commit/909a47f9d888d9f1411ef63ed1b5a62f602823e6))
* default example only appear when no doc ([b3b4156](https://github.com/vue-styleguidist/vue-styleguidist/commit/b3b4156c726799e40015f0bc57670085a63518af))
* default example parser with alias ([361051c](https://github.com/vue-styleguidist/vue-styleguidist/commit/361051c2108cb471317721dce5917f044d446fff)), closes [#806](https://github.com/vue-styleguidist/vue-styleguidist/issues/806)
* defaultConfig ([#186](https://github.com/vue-styleguidist/vue-styleguidist/issues/186)) ([10900a1](https://github.com/vue-styleguidist/vue-styleguidist/commit/10900a1146bc0bc138d10991e18dfb4b956c8b03))
* delegated component exports ([046f96b](https://github.com/vue-styleguidist/vue-styleguidist/commit/046f96bc1e9d6d4f9514b74c2666710cf09019a5))
* dependencies were extracted and vue-webpack-loaders updated ([babf7f4](https://github.com/vue-styleguidist/vue-styleguidist/commit/babf7f43d0ffe96ec7a3067a5f2d4475d067375f))
* dependency update for security ([e227276](https://github.com/vue-styleguidist/vue-styleguidist/commit/e2272766ec4469ba4fd00bb6a7e9f288e3171c75))
* **deps:** update all ([78807c3](https://github.com/vue-styleguidist/vue-styleguidist/commit/78807c34893be6746b4f18e0d3d1d706a4e816af))
* destroy Vue component in Preview when replacing it or unmounting ([00b7658](https://github.com/vue-styleguidist/vue-styleguidist/commit/00b7658da38abc38f687ec42452524d5ed3987ff))
* detect pure template no script as sfc ([e2a0a48](https://github.com/vue-styleguidist/vue-styleguidist/commit/e2a0a484fb033b8d00c8ef92dcd73dd39690f62f))
* display show code when only events ([aeea160](https://github.com/vue-styleguidist/vue-styleguidist/commit/aeea1608774f87e35aafff09c41944ff6730833e)), closes [#801](https://github.com/vue-styleguidist/vue-styleguidist/issues/801)
* **docgen:**  fix ts array, intersection type print ([4ca38bf](https://github.com/vue-styleguidist/vue-styleguidist/commit/4ca38bf6a6e5fd39e87276332461c24ddb01cc5a))
* **docgen:** accept named typescript exports ([b256b17](https://github.com/vue-styleguidist/vue-styleguidist/commit/b256b174347766d31ab55fab948849037f32d930)), closes [#813](https://github.com/vue-styleguidist/vue-styleguidist/issues/813)
* **docgen:** adapt method handler to default params ([4f67f4e](https://github.com/vue-styleguidist/vue-styleguidist/commit/4f67f4eefbd32c1ad73fca464b1ebd20cf397c81)), closes [#476](https://github.com/vue-styleguidist/vue-styleguidist/issues/476)
* **docgen:** allow `as const` in default ([d3f070d](https://github.com/vue-styleguidist/vue-styleguidist/commit/d3f070dd08677a6614498ff8de8c91ea287c75bf))
* **docgen:** allow base indent in pug templates ([0950074](https://github.com/vue-styleguidist/vue-styleguidist/commit/09500746aa648b03a59550f7e591aa5243727612)), closes [#979](https://github.com/vue-styleguidist/vue-styleguidist/issues/979)
* **docgen:** allow default to be a method ([40ec2ae](https://github.com/vue-styleguidist/vue-styleguidist/commit/40ec2ae76a81ae90894539e48b9c501c8d22f090))
* **docgen:** allow default to be a string key ([1fa756f](https://github.com/vue-styleguidist/vue-styleguidist/commit/1fa756f02d194aaa303a424538a3d681a177a522)), closes [#581](https://github.com/vue-styleguidist/vue-styleguidist/issues/581)
* **docgen:** allow document scopedSlots in render ([31a7e07](https://github.com/vue-styleguidist/vue-styleguidist/commit/31a7e071ddd4a443db2f8cd7bde11c6efe8bc6a9)), closes [#174](https://github.com/vue-styleguidist/vue-styleguidist/issues/174)
* **docgen:** allow export - as - from "-" ([f7ac47c](https://github.com/vue-styleguidist/vue-styleguidist/commit/f7ac47cf89667e389670c11f44f3edc7e1cdfd0f)), closes [#911](https://github.com/vue-styleguidist/vue-styleguidist/issues/911)
* **docgen:** allow for multiple times the same tag ([68a0204](https://github.com/vue-styleguidist/vue-styleguidist/commit/68a020494b9f2c6f227e1305056d91c4f4d92d6b))
* **docgen:** allow for not parsing jsx ([8b669f3](https://github.com/vue-styleguidist/vue-styleguidist/commit/8b669f3d8676afe3b4e3c226020dbd8b0507baa8))
* **docgen:** allow for v-model in functional components ([8884e62](https://github.com/vue-styleguidist/vue-styleguidist/commit/8884e62e029ca77af91a510cb756415e6de6775a)), closes [#493](https://github.com/vue-styleguidist/vue-styleguidist/issues/493)
* **docgen:** allow functional render slots ([2b36e38](https://github.com/vue-styleguidist/vue-styleguidist/commit/2b36e38042fb4836eb8afb68df3ebf74cac3b8e3)), closes [#837](https://github.com/vue-styleguidist/vue-styleguidist/issues/837)
* **docgen:** allow Mixins from vue-p-decorator ([314686a](https://github.com/vue-styleguidist/vue-styleguidist/commit/314686a1f91dec4ef246b809d217d8ee0dc5466a))
* **docgen:** allow multi line root comment ([c6eacf7](https://github.com/vue-styleguidist/vue-styleguidist/commit/c6eacf72ffd60a21ca45248951076464264f5ea4))
* **docgen:** allow single slots to not documented ([34381d1](https://github.com/vue-styleguidist/vue-styleguidist/commit/34381d162f914f2ad94f382acb09227365ec4fc0))
* **docgen:** allow TypeScript to export a constant ([353601e](https://github.com/vue-styleguidist/vue-styleguidist/commit/353601ea21da33b237c52bf0ba376d6c3f32d9aa)), closes [#997](https://github.com/vue-styleguidist/vue-styleguidist/issues/997)
* **docgen:** avoid incorrect of getting nested '}' param type ([5df05e0](https://github.com/vue-styleguidist/vue-styleguidist/commit/5df05e0a8e2540ae0af882d83daf4f28d818b241))
* **docgen:** avoid outputing empty array ([51d42bf](https://github.com/vue-styleguidist/vue-styleguidist/commit/51d42bf4d987f78f51a0fc88d50f5faa832321c0))
* **docgen:** avoid parse files that are'nt potential components ([4b1e43b](https://github.com/vue-styleguidist/vue-styleguidist/commit/4b1e43b253c5674e7a982607256396dd47d03270)), closes [#436](https://github.com/vue-styleguidist/vue-styleguidist/issues/436)
* **docgen:** avoid setting exportName to deps ([230e1e3](https://github.com/vue-styleguidist/vue-styleguidist/commit/230e1e338bc53c1b73f504163a10e56ff9a374e1))
* **docgen:** correctly extract default values ([349ad81](https://github.com/vue-styleguidist/vue-styleguidist/commit/349ad812894120e1c4816df55a427a8724b99b8c))
* **docgen:** detetct scopedSlots in render() ([5e7015d](https://github.com/vue-styleguidist/vue-styleguidist/commit/5e7015d5e2870de1fa4c11b44d5434e0c5b3d946)), closes [#586](https://github.com/vue-styleguidist/vue-styleguidist/issues/586)
* **docgen:** docs only vue components ([fcc28f6](https://github.com/vue-styleguidist/vue-styleguidist/commit/fcc28f6f330736b565bd3343422a2cc8792f8200)), closes [#731](https://github.com/vue-styleguidist/vue-styleguidist/issues/731)
* **docgen:** ensure custom handlers are actually run ([7a0ac62](https://github.com/vue-styleguidist/vue-styleguidist/commit/7a0ac6270a58964489b902a530bd08a9bce935f4))
* **docgen:** ensure pug renders template using html doctype ([2f3512b](https://github.com/vue-styleguidist/vue-styleguidist/commit/2f3512b7951889c8fa72856655725b62bd4e81fb))
* **docgen:** error when parsing default prop ([1fe3dfe](https://github.com/vue-styleguidist/vue-styleguidist/commit/1fe3dfeeaccdf2c1d5d6b2033b0b1ac8db510103))
* **docgen:** export iev var names ([c02268b](https://github.com/vue-styleguidist/vue-styleguidist/commit/c02268b31fd34f8e1cabd4f149f2e8dd30ff0ee3))
* **docgen:** filter components more clearly ([09b15e9](https://github.com/vue-styleguidist/vue-styleguidist/commit/09b15e9824dd7687ceb8bd94455c4ed5870b3214)), closes [#735](https://github.com/vue-styleguidist/vue-styleguidist/issues/735)
* **docgen:** fix default parsing for [@type](https://github.com/type) props ([1fc4b03](https://github.com/vue-styleguidist/vue-styleguidist/commit/1fc4b03eb408f535f7bb8e85bb8037800aee2eb1)), closes [#866](https://github.com/vue-styleguidist/vue-styleguidist/issues/866)
* **docgen:** fix node_modules mixins parsing ([a4eed84](https://github.com/vue-styleguidist/vue-styleguidist/commit/a4eed8452ab9b571d58c1beeaa5b8a63dfcb0d82)), closes [#416](https://github.com/vue-styleguidist/vue-styleguidist/issues/416)
* **docgen:** fix strict alias type to support arrays ([47c6f7b](https://github.com/vue-styleguidist/vue-styleguidist/commit/47c6f7b022aa919324e3b21ac760d155eb47cfc1))
* **docgen:** fix template parsing expressions ([56a2e05](https://github.com/vue-styleguidist/vue-styleguidist/commit/56a2e05c85f0b6d80855aae04115a7194e94f872))
* **docgen:** fixed description extraction on non-SFC components ([85626fc](https://github.com/vue-styleguidist/vue-styleguidist/commit/85626fc7209013507475d330c87be9ee52f63cba))
* **docgen:** fixed multiple use of same event needing desc ([329f66a](https://github.com/vue-styleguidist/vue-styleguidist/commit/329f66a33b5d6dd8334a7dc2e8bd312ddbbe962e)), closes [#459](https://github.com/vue-styleguidist/vue-styleguidist/issues/459)
* **docgen:** get slot and scoped slot description in render without JSX ([33086cf](https://github.com/vue-styleguidist/vue-styleguidist/commit/33086cf475fd3088e5b9100990be95d9e5685dfd))
* **docgen:** give better error message lines ([9b04cc4](https://github.com/vue-styleguidist/vue-styleguidist/commit/9b04cc479376bf6755930071c4e71b6dd7ba19c5))
* **docgen:** handle empty handler ([f811ed3](https://github.com/vue-styleguidist/vue-styleguidist/commit/f811ed3d6d16be36eb10071dd15381d8008e54fd)), closes [#738](https://github.com/vue-styleguidist/vue-styleguidist/issues/738)
* **docgen:** handlers expressions with multiline ([8e7c66c](https://github.com/vue-styleguidist/vue-styleguidist/commit/8e7c66c62388746579ddda3753aa743b41a88c43)), closes [#772](https://github.com/vue-styleguidist/vue-styleguidist/issues/772)
* **docgen:** html doctype as a default rather than a force ([01c921f](https://github.com/vue-styleguidist/vue-styleguidist/commit/01c921f8054607b9e0d99c0f87a4820fe8dedc2d))
* **docgen:** keep comments in the template in prod ([b9e4a89](https://github.com/vue-styleguidist/vue-styleguidist/commit/b9e4a89817a91731a40671760f7c7dc482090a25)), closes [#942](https://github.com/vue-styleguidist/vue-styleguidist/issues/942)
* **docgen:** make aliases only path parts instead of letters ([b83e235](https://github.com/vue-styleguidist/vue-styleguidist/commit/b83e235e942acb420c16aaa385728853df89cb41)), closes [#478](https://github.com/vue-styleguidist/vue-styleguidist/issues/478)
* **docgen:** make displayName tag work ([#358](https://github.com/vue-styleguidist/vue-styleguidist/issues/358)) ([c3e12ce](https://github.com/vue-styleguidist/vue-styleguidist/commit/c3e12ceaddcb57b288748f9af4acf8f886b31a85)), closes [#357](https://github.com/vue-styleguidist/vue-styleguidist/issues/357)
* **docgen:** make docgen output arrays only ([d456c6c](https://github.com/vue-styleguidist/vue-styleguidist/commit/d456c6cc4d01f2a81879a2720135617f339bd487))
* **docgen:** make events parsed in template ([e361bef](https://github.com/vue-styleguidist/vue-styleguidist/commit/e361bef34cdc78baf08f12cf69eb17069af49527))
* **docgen:** make sure v-slot templates are understood too ([e9ab6d5](https://github.com/vue-styleguidist/vue-styleguidist/commit/e9ab6d59f137fad8681f7cd52842161fc9d3da68))
* **docgen:** make v-bind have a separate treatment ([cee2a9b](https://github.com/vue-styleguidist/vue-styleguidist/commit/cee2a9ba608d2c4da9998b488a0b9ba27d6aad0c)), closes [#469](https://github.com/vue-styleguidist/vue-styleguidist/issues/469)
* **docgen:** Multicephal templates ([9327271](https://github.com/vue-styleguidist/vue-styleguidist/commit/93272711d6256b1d1f629a5bb4e4b3e7b94e7493)), closes [#322](https://github.com/vue-styleguidist/vue-styleguidist/issues/322)
* **docgen:** multiple class component in same file ([cb0e986](https://github.com/vue-styleguidist/vue-styleguidist/commit/cb0e986ac2f43fa6e6b1f651ddee908e0f4f4aaa)), closes [#1130](https://github.com/vue-styleguidist/vue-styleguidist/issues/1130)
* **docgen:** object Methods should return regular functions ([79a7fa2](https://github.com/vue-styleguidist/vue-styleguidist/commit/79a7fa2271d93dc71c6651e262ea25a244ee656a))
* **docgen:** origin never cleared ([0dc4251](https://github.com/vue-styleguidist/vue-styleguidist/commit/0dc42513e83a46323ffb4227e4a0b5d485ca06cd))
* **docgen:** parse bindings with a dash in jsdoc ([b7b0d30](https://github.com/vue-styleguidist/vue-styleguidist/commit/b7b0d30fa8803505873f176ffda6b63ba473c417)), closes [#1229](https://github.com/vue-styleguidist/vue-styleguidist/issues/1229)
* **docgen:** priority to documented values ([696bd87](https://github.com/vue-styleguidist/vue-styleguidist/commit/696bd873a914a4d5057f3dda27f3f9f7eaffa0a2))
* **docgen:** protect empty comments before slots ([6484a10](https://github.com/vue-styleguidist/vue-styleguidist/commit/6484a106b2964c5f1858171ad4ad40642a4f98b9)), closes [#749](https://github.com/vue-styleguidist/vue-styleguidist/issues/749)
* **docgen:** protect uresolved events ([09d970f](https://github.com/vue-styleguidist/vue-styleguidist/commit/09d970f781f83e7e85b8858e5b909d9a35c1b6ae)), closes [#363](https://github.com/vue-styleguidist/vue-styleguidist/issues/363)
* **docgen:** remove TSUnionType ([2f32e5f](https://github.com/vue-styleguidist/vue-styleguidist/commit/2f32e5f4b6972610a7406d98601b7faa2ac7d354)), closes [#796](https://github.com/vue-styleguidist/vue-styleguidist/issues/796)
* **docgen:** resolve es6 modules properly ([1b4eb0a](https://github.com/vue-styleguidist/vue-styleguidist/commit/1b4eb0aec0fe1e5883ccabb3f98506e4000af68e)), closes [#478](https://github.com/vue-styleguidist/vue-styleguidist/issues/478)
* **docgen:** resolve exported displayname ([d414c4a](https://github.com/vue-styleguidist/vue-styleguidist/commit/d414c4a137f012160fdcdb585f46908bb942dd8a)), closes [#1220](https://github.com/vue-styleguidist/vue-styleguidist/issues/1220)
* **docgen:** scoped slots bindings can spread ([d0a939c](https://github.com/vue-styleguidist/vue-styleguidist/commit/d0a939c4edb9fdd2986a5695e9894c462be01e36)), closes [#833](https://github.com/vue-styleguidist/vue-styleguidist/issues/833)
* **docgen:** SFC with lang='tsx' support ([bd21931](https://github.com/vue-styleguidist/vue-styleguidist/commit/bd2193199988b786aae79a2e35aa552c1e5f8f54))
* **docgen:** support [@values](https://github.com/values) on classPropHandler ([4b7f8b6](https://github.com/vue-styleguidist/vue-styleguidist/commit/4b7f8b6e55d49cc5d27ca9c2c47abd18edcc965f))
* **docgen:** take mixin order into account ([626337e](https://github.com/vue-styleguidist/vue-styleguidist/commit/626337ebc2bce975b16815971f73f3a6f7b7a9b8)), closes [#761](https://github.com/vue-styleguidist/vue-styleguidist/issues/761)
* **docgen:** template was used to use slots - sfc was detected ([642d875](https://github.com/vue-styleguidist/vue-styleguidist/commit/642d8750de88e6dcef36f0ce99ccb1ce20f6064f)), closes [#448](https://github.com/vue-styleguidist/vue-styleguidist/issues/448)
* **docgen:** update method for unpassing tests ([4f5c6cd](https://github.com/vue-styleguidist/vue-styleguidist/commit/4f5c6cde1c02beb7bb124c7e00a46aaa26b1164a))
* **docgen:** use the pathResolver from utils ([3b77a82](https://github.com/vue-styleguidist/vue-styleguidist/commit/3b77a8240fe16451d660b57584d4178b018fc6c7))
* **docgen:** use webpack modules when resolving paths ([6b5b87f](https://github.com/vue-styleguidist/vue-styleguidist/commit/6b5b87f65e219ce5798ac0ea044271a25d6ad086)), closes [#743](https://github.com/vue-styleguidist/vue-styleguidist/issues/743)
* **docgen:** Vue.extends without comments ([bf42ccc](https://github.com/vue-styleguidist/vue-styleguidist/commit/bf42ccc05c8790a074d979af9f78bc50f898dccd)), closes [#1027](https://github.com/vue-styleguidist/vue-styleguidist/issues/1027)
* **docs:** Correct netlify build command ([92dc3be](https://github.com/vue-styleguidist/vue-styleguidist/commit/92dc3bee6ad6d08a1dfc6e9a011ea98db372664d))
* downgrade acorn ([40b60cb](https://github.com/vue-styleguidist/vue-styleguidist/commit/40b60cb583d6a2a45aabd584487bd15e310b1e74))
* editor should update when changing page ([35d0c3f](https://github.com/vue-styleguidist/vue-styleguidist/commit/35d0c3f9bcd4b0f835a7dab9365fa4dbf1f4a499))
* **editor:** html was rendered automaticaly by mistake ([a304c86](https://github.com/vue-styleguidist/vue-styleguidist/commit/a304c861c5f3a5859c2a66d628c81850d4a147c8)), closes [#234](https://github.com/vue-styleguidist/vue-styleguidist/issues/234)
* **editor:** make sure when url changes editor is repainted ([2dcbaac](https://github.com/vue-styleguidist/vue-styleguidist/commit/2dcbaacc74a57607c915b5516105640de8ded7e9)), closes [#404](https://github.com/vue-styleguidist/vue-styleguidist/issues/404)
* EditorWithToolbar naming case ([595a077](https://github.com/vue-styleguidist/vue-styleguidist/commit/595a077fc9a44609055ba30aad55ba3bc095242c))
* Enable serverPort config to accept string ([#276](https://github.com/vue-styleguidist/vue-styleguidist/issues/276)) ([00baa20](https://github.com/vue-styleguidist/vue-styleguidist/commit/00baa200d77fc679390c1da8af995deb1ea74d43)), closes [#275](https://github.com/vue-styleguidist/vue-styleguidist/issues/275)
* error mini-css-extract-plugin ([1683e6c](https://github.com/vue-styleguidist/vue-styleguidist/commit/1683e6c6d68f16ae28dd3b5fbd64abcfe818b986))
* error typing errors (so meta) ([4e4333f](https://github.com/vue-styleguidist/vue-styleguidist/commit/4e4333f5d7bb93c8bdfa73835d757bc3fe3b80dc))
* Error when trying build with vue-cli ([8bb5c37](https://github.com/vue-styleguidist/vue-styleguidist/commit/8bb5c373f8a8d0b3e6e17c3d44557a85156bb6f6))
* **eslint:** remove no-console exception in vuedoc-loader ([72464ed](https://github.com/vue-styleguidist/vue-styleguidist/commit/72464ed3f9c2a65d440c2e580f07f65b372616ea))
* **examples:** allow multiple root tags in examples ([3c93cb5](https://github.com/vue-styleguidist/vue-styleguidist/commit/3c93cb5d43f843a42c1e2115863c46db6b425822)), closes [#259](https://github.com/vue-styleguidist/vue-styleguidist/issues/259)
* expected corresponding JSX closing tag for <br> ([85d1efd](https://github.com/vue-styleguidist/vue-styleguidist/commit/85d1efdf0a0f2e684a5207c9e3e2240bab5a08aa))
* extend quoting to methods and props ([10e2b3e](https://github.com/vue-styleguidist/vue-styleguidist/commit/10e2b3e71fd416fabecef1f5baff14e98fcb855b))
* findSection for ie11 [#83](https://github.com/vue-styleguidist/vue-styleguidist/issues/83) ([6dab4db](https://github.com/vue-styleguidist/vue-styleguidist/commit/6dab4db548782258b6be65ffed4a2bc1b82caf2d))
* Fix bad links to docs ([a5a8978](https://github.com/vue-styleguidist/vue-styleguidist/commit/a5a8978218f149bb4eaa2270e4f89393ae4f0ed8))
* Fix validation error for uglifyjs-webpack-plugin [#34](https://github.com/vue-styleguidist/vue-styleguidist/issues/34) ([b6f378e](https://github.com/vue-styleguidist/vue-styleguidist/commit/b6f378e5078a8749b940bcfbc1b1a28b7896150a))
* Fixed unix [#36](https://github.com/vue-styleguidist/vue-styleguidist/issues/36) ([85073c1](https://github.com/vue-styleguidist/vue-styleguidist/commit/85073c1348532b5ad32497da5a13633650966e2e))
* **git:** fixup gitattributes ([628fca5](https://github.com/vue-styleguidist/vue-styleguidist/commit/628fca5dc1df2495ae3ebd5ed4ec2e4399842c82))
* **gitter readme:** gitter badge on top of readme ([50198f6](https://github.com/vue-styleguidist/vue-styleguidist/commit/50198f6d0a0f32804ce8453e5ec21a4c32e59c87))
* hint user when wrong version of vue installed ([b1af242](https://github.com/vue-styleguidist/vue-styleguidist/commit/b1af242ac66a721fb39859b189f9f39f597e1876))
* hot reload default examples ([295cfe5](https://github.com/vue-styleguidist/vue-styleguidist/commit/295cfe553ce8d5e48a3c7607672017246d65d992))
* if cwd is specified in cmd use to find config ([2bf97a1](https://github.com/vue-styleguidist/vue-styleguidist/commit/2bf97a12c22431ea44b5a8efc5ba4f890e522ea3))
* if using cli and using rules with use as a table ([037e4f8](https://github.com/vue-styleguidist/vue-styleguidist/commit/037e4f8d945896112d76ff5fd7c7411d885d3db5))
* ignored errors ([#177](https://github.com/vue-styleguidist/vue-styleguidist/issues/177)) ([#316](https://github.com/vue-styleguidist/vue-styleguidist/issues/316)) ([298f462](https://github.com/vue-styleguidist/vue-styleguidist/commit/298f46264ec89be3c592484e36afdd009c3ce3a7))
* implement webpack resolve feature in vsg ([#274](https://github.com/vue-styleguidist/vue-styleguidist/issues/274)) ([c9f89f9](https://github.com/vue-styleguidist/vue-styleguidist/commit/c9f89f917e56c160b9996e8358c9362004ea8304)), closes [#273](https://github.com/vue-styleguidist/vue-styleguidist/issues/273)
* import issue conflicts with babel ([f1ac618](https://github.com/vue-styleguidist/vue-styleguidist/commit/f1ac618b7d571411303ada1a31165e12eac00b9d)), closes [#635](https://github.com/vue-styleguidist/vue-styleguidist/issues/635)
* import of named mixins failing ([185fb22](https://github.com/vue-styleguidist/vue-styleguidist/commit/185fb229d5404ed5e26e319b499cf13b1e3a5a8a))
* issues with deprecated and requires ([efa53c9](https://github.com/vue-styleguidist/vue-styleguidist/commit/efa53c98d8fefdcc51ffeee65a99948e1f7700eb)), closes [#870](https://github.com/vue-styleguidist/vue-styleguidist/issues/870) [#871](https://github.com/vue-styleguidist/vue-styleguidist/issues/871)
* keep dashes in component names ([3ec75ed](https://github.com/vue-styleguidist/vue-styleguidist/commit/3ec75ed58454b0d77745233e4e6b300317ea9f76)), closes [#391](https://github.com/vue-styleguidist/vue-styleguidist/issues/391)
* **lerna:** force delivery of docgen after lerna breakdown ([2a90af6](https://github.com/vue-styleguidist/vue-styleguidist/commit/2a90af690eb96cdccb4237b67960266f7e8902f8))
* load export const name = Vue.extends all good ([2e760c9](https://github.com/vue-styleguidist/vue-styleguidist/commit/2e760c9691a9b7c927341afd4407dfa2b11f055c)), closes [#1069](https://github.com/vue-styleguidist/vue-styleguidist/issues/1069)
* Load styles with vue-cli3 ([#166](https://github.com/vue-styleguidist/vue-styleguidist/issues/166)) and styleguideDir([#159](https://github.com/vue-styleguidist/vue-styleguidist/issues/159)) ([9896999](https://github.com/vue-styleguidist/vue-styleguidist/commit/9896999ad611869f78141de6682fab7b73298309))
* **loaders/utilsprocessComponent:** wrong error when trying to doc a js file ([96422bb](https://github.com/vue-styleguidist/vue-styleguidist/commit/96422bbc7eb4158e90975f0b7f97707345188a2d))
* **loaders:** fixed error message display when docgen errors ([f6ac222](https://github.com/vue-styleguidist/vue-styleguidist/commit/f6ac222c95c27ba586198affd3b32977fa666f2a)), closes [#309](https://github.com/vue-styleguidist/vue-styleguidist/issues/309)
* **loaders:** remove useless error when wrong extension ([22ca8b0](https://github.com/vue-styleguidist/vue-styleguidist/commit/22ca8b0da11f7a45d10a1e78550a040a4af574cc))
* **loaders:** remove useless error when wrong extension ([a90bc40](https://github.com/vue-styleguidist/vue-styleguidist/commit/a90bc40831cb768843eec4b35827caa0f65fb284))
* **loader:** use emit error to thow parsing errors ([8978f54](https://github.com/vue-styleguidist/vue-styleguidist/commit/8978f54c85f3144eb4ab1b3a46fd65f801f8031d))
* lock dependencies as they are locked in rsg ([f418e07](https://github.com/vue-styleguidist/vue-styleguidist/commit/f418e07278a49d9717906aaf7665362effd76530))
* look at statements for description ([71969bf](https://github.com/vue-styleguidist/vue-styleguidist/commit/71969bfec9b031873ce351c7c5e968041715c2a7))
* Look through all roots. ([3641e4c](https://github.com/vue-styleguidist/vue-styleguidist/commit/3641e4c7a210f79c9c91da616e2f20803de15a86))
* make (Vue as VueConstructor<Vue>) resolved ([b7ed624](https://github.com/vue-styleguidist/vue-styleguidist/commit/b7ed624f002e22894cbf698e955b9765c94dcdaa))
* make [@arg](https://github.com/arg) and [@argument](https://github.com/argument) accepted ([1b0ddca](https://github.com/vue-styleguidist/vue-styleguidist/commit/1b0ddcad52555366307d768c1557b2c87ad55bd4))
* make codeSplit comptible with jsxInExamples ([83c0bf6](https://github.com/vue-styleguidist/vue-styleguidist/commit/83c0bf6deb1b98b5d8cdcdb9ee28dbba096e7c27))
* make hidden components work again ([4898fee](https://github.com/vue-styleguidist/vue-styleguidist/commit/4898feec3c97888a3d8d717518f221ae32416ac4))
* make interface for codetabbutton exported ([ccd52d8](https://github.com/vue-styleguidist/vue-styleguidist/commit/ccd52d83ed7fafa6faf2f7ad9e31ff7ef0fd375f))
* make library compatible with codesandbox ([c4b531d](https://github.com/vue-styleguidist/vue-styleguidist/commit/c4b531db8d00f31eb9aad6aa240cb3ab1415541e))
* make origin column smaller ([4b7027e](https://github.com/vue-styleguidist/vue-styleguidist/commit/4b7027ec27a03c8be588d93c3d2e8772e07d45d9))
* make regexp more precise ([29ba8b5](https://github.com/vue-styleguidist/vue-styleguidist/commit/29ba8b596f3c093bf40fe1675717b72d389bc7c1))
* make sections without examples pre-compile ([56d675d](https://github.com/vue-styleguidist/vue-styleguidist/commit/56d675d906b4cf76861f7b6f819a6642a19f90cd))
* make sure code split works with prism ([51e7660](https://github.com/vue-styleguidist/vue-styleguidist/commit/51e7660b84dbd03d120e604af29d9dd2c6cddd0f))
* make sure defaults are there for the plugin ([eb9ef4c](https://github.com/vue-styleguidist/vue-styleguidist/commit/eb9ef4cbb9dd175b1d51b9a7ec6e605834515e43)), closes [#615](https://github.com/vue-styleguidist/vue-styleguidist/issues/615)
* make sure defaults are there for the plugin ([00a05ac](https://github.com/vue-styleguidist/vue-styleguidist/commit/00a05ac4e9c2e5e2804d7da95a5f4348d2bcd1eb)), closes [#615](https://github.com/vue-styleguidist/vue-styleguidist/issues/615)
* make sure imported variables are declared ([bc50ab1](https://github.com/vue-styleguidist/vue-styleguidist/commit/bc50ab104edb29b233502e71b2943267870b2d64))
* make sure node_module resolved path ignored ([7a1092a](https://github.com/vue-styleguidist/vue-styleguidist/commit/7a1092a3a636c302a6b83a7446da1d86b49cb593))
* make sure we detect all variables ([118f1a8](https://github.com/vue-styleguidist/vue-styleguidist/commit/118f1a81df9143f748f1f14a83281f34a6190efa))
* make types work better ([60c2d32](https://github.com/vue-styleguidist/vue-styleguidist/commit/60c2d32981ef20f4567bd9700667bdc9df3151bb))
* make usage table font sizes and spacing consistent ([7fc22fe](https://github.com/vue-styleguidist/vue-styleguidist/commit/7fc22fe1d78a4f8c0c02cfdefe60feca1565254b))
* makes vue alias path absolute ([#284](https://github.com/vue-styleguidist/vue-styleguidist/issues/284)) ([9c18220](https://github.com/vue-styleguidist/vue-styleguidist/commit/9c182201c89daf4c325fd2102d86fc8ff59c4557)), closes [#285](https://github.com/vue-styleguidist/vue-styleguidist/issues/285)
* mandatory rules cli ([db20575](https://github.com/vue-styleguidist/vue-styleguidist/commit/db205758ab104fd8ace486dd388f54e7fdc79f79))
* mixed scoped and non-scoped slots render ([4161ff2](https://github.com/vue-styleguidist/vue-styleguidist/commit/4161ff233e340befdecafe1db520e0918858d640))
* Mixins aren't apply [#80](https://github.com/vue-styleguidist/vue-styleguidist/issues/80) ([ca81ba8](https://github.com/vue-styleguidist/vue-styleguidist/commit/ca81ba84c52f87dbed303f4ac24b1d4e1140efc1))
* move the build files to the right folder ([9944972](https://github.com/vue-styleguidist/vue-styleguidist/commit/99449722c2da8d099d1097dbe58387fa3d1a7920)), closes [#615](https://github.com/vue-styleguidist/vue-styleguidist/issues/615)
* move the build files to the right folder ([3b5aea1](https://github.com/vue-styleguidist/vue-styleguidist/commit/3b5aea13d80029f41a1aecb3faef3c9d7be41a77)), closes [#615](https://github.com/vue-styleguidist/vue-styleguidist/issues/615)
* multiple exports in parse export default ([7bb82dd](https://github.com/vue-styleguidist/vue-styleguidist/commit/7bb82ddb3e8173dccc344117a4a2e50b42360639))
* multiple exports in vue files ([56fcdd8](https://github.com/vue-styleguidist/vue-styleguidist/commit/56fcdd8f0642347f16e88dd04b2d4c7ec8aef9c6)), closes [#717](https://github.com/vue-styleguidist/vue-styleguidist/issues/717)
* new Vue compatible with live require ([f3f68a8](https://github.com/vue-styleguidist/vue-styleguidist/commit/f3f68a83651c12d0293a395cc7f997c1eb2e8f36)), closes [#702](https://github.com/vue-styleguidist/vue-styleguidist/issues/702)
* no undefined consts ([9e28ebf](https://github.com/vue-styleguidist/vue-styleguidist/commit/9e28ebf18b4e0cea1341756d45747b1c8173af48))
* node types incompatible with typescript ([28e9f68](https://github.com/vue-styleguidist/vue-styleguidist/commit/28e9f681059ae06ea9566ec00cb158b4491f858a))
* only collapse subcomponents when > 3 ([76b4331](https://github.com/vue-styleguidist/vue-styleguidist/commit/76b43316a63d40027e2345ee855d9aee25836493))
* only display the existing API titles ([653cc18](https://github.com/vue-styleguidist/vue-styleguidist/commit/653cc181b99830c15859327ed93369eb4dfb3166))
* only display warning when file exists ([cfe1f9f](https://github.com/vue-styleguidist/vue-styleguidist/commit/cfe1f9faa9f109413ee7f59f611fed9a2d37579e))
* only show required props on default examples ([0f6bc11](https://github.com/vue-styleguidist/vue-styleguidist/commit/0f6bc1188cff5d4781bebb1ddef48d6b0f9482b2))
* **options:** allow two words in displayName ([7b72603](https://github.com/vue-styleguidist/vue-styleguidist/commit/7b72603a929c0d046a4379e1e8100832b29953c4))
* pagePerSection when only top section exists ([70cbb59](https://github.com/vue-styleguidist/vue-styleguidist/commit/70cbb593cbbb6f94f300c5c1ddad69c4d38e81e7)), closes [#1054](https://github.com/vue-styleguidist/vue-styleguidist/issues/1054)
* pages infinite lioop ([9e5acad](https://github.com/vue-styleguidist/vue-styleguidist/commit/9e5acada9a9c7cafeb65cf8f1b5792c18c61482b))
* parse es6 imports with vsg format ([8f5ff19](https://github.com/vue-styleguidist/vue-styleguidist/commit/8f5ff194639bcb8e1e1f93f57c99af8d4404478c))
* parse should export default cmp if available ([753dea4](https://github.com/vue-styleguidist/vue-styleguidist/commit/753dea4c26a3918488b08b32bfe8b7dbea109f60))
* parsing of validator with a function ref ([17472c3](https://github.com/vue-styleguidist/vue-styleguidist/commit/17472c3a26a2b43da26de6169388b35f8338195f)), closes [#954](https://github.com/vue-styleguidist/vue-styleguidist/issues/954)
* passing a webpackConfig should prioitize ([683f3dc](https://github.com/vue-styleguidist/vue-styleguidist/commit/683f3dc30f73d8edac4cef825117537f8bdf71d1))
* **plugin/ui:** mountPointId link was wrong ([#348](https://github.com/vue-styleguidist/vue-styleguidist/issues/348)) ([5d79ebb](https://github.com/vue-styleguidist/vue-styleguidist/commit/5d79ebbc4ac63f11664335591a36b6fb00fcb3de))
* **plugin/ui:** use es5 to module ui ([4e774a2](https://github.com/vue-styleguidist/vue-styleguidist/commit/4e774a28588d32975100699a9e8b51a00fae93f2))
* **plugin:** add the whole package to eslintignore ([3b13ccf](https://github.com/vue-styleguidist/vue-styleguidist/commit/3b13ccf85cd7634495579b57a98ebdcfafc90a0c))
* **plugin:** avoid fork-ts success notification ([9ac7a09](https://github.com/vue-styleguidist/vue-styleguidist/commit/9ac7a092f1f4857ee7396105507af04cea67c986))
* **plugin:** custom webpack config ([2cf491c](https://github.com/vue-styleguidist/vue-styleguidist/commit/2cf491c16c09a610c78b62639107c0502640dd5c))
* **plugin:** default custom components ([9c45104](https://github.com/vue-styleguidist/vue-styleguidist/commit/9c45104a86ef1f59fc8c24a0ecfa2d46b2d3718c))
* **plugin:** es6 requires fix in plugin ([205f7a1](https://github.com/vue-styleguidist/vue-styleguidist/commit/205f7a1f5dfb92760c217b09d2495f8a9cda80e1))
* **plugin:** issue with babel ([afbf21a](https://github.com/vue-styleguidist/vue-styleguidist/commit/afbf21a689c30a7ae49b3abd90d8d7a1c03950b4))
* **plugin:** load styleguidist right ([d4c6f6d](https://github.com/vue-styleguidist/vue-styleguidist/commit/d4c6f6d6e2d4418e60439428e28cf4609b799eae))
* **plugin:** make cli plugin ok with production builds ([429ed55](https://github.com/vue-styleguidist/vue-styleguidist/commit/429ed555a4b5e9707a2caf9e12a99e5a02ebcf00))
* **plugin:** pass cli options down to vsg ([6765aee](https://github.com/vue-styleguidist/vue-styleguidist/commit/6765aee4a72b8ab36eb0094ead8a92d7b4a6789c)), closes [#771](https://github.com/vue-styleguidist/vue-styleguidist/issues/771)
* **plugin:** update null-loader + avoid conflicts ([07f6a98](https://github.com/vue-styleguidist/vue-styleguidist/commit/07f6a98dc391a02d503c62debcfd3b8b7950d0e9))
* **plugin:** use a custom loader to deal with docs ([57abe6e](https://github.com/vue-styleguidist/vue-styleguidist/commit/57abe6e7c4ae34f7b0c26d5a8fe834b5a8133b8b))
* pre classname warning ([b80f97a](https://github.com/vue-styleguidist/vue-styleguidist/commit/b80f97a9818410b46ec03552d27b2b59b18531d8))
* precompile examples when codeSplit ([d75f3f4](https://github.com/vue-styleguidist/vue-styleguidist/commit/d75f3f49bee7b2d497adf45d7d2f38a9ab96e463))
* **preview:** fix style scope mismatch ([830abf8](https://github.com/vue-styleguidist/vue-styleguidist/commit/830abf8311cce48b8b40328b41d595f458335492)), closes [#437](https://github.com/vue-styleguidist/vue-styleguidist/issues/437)
* **preview:** gracefully fail when Vue breaks ([1152600](https://github.com/vue-styleguidist/vue-styleguidist/commit/11526000bbfeb0340d1f71c13d99e51b562b96f1)), closes [#435](https://github.com/vue-styleguidist/vue-styleguidist/issues/435)
* progressbar assumed custom webpack ([96e28e6](https://github.com/vue-styleguidist/vue-styleguidist/commit/96e28e64ff139922ce5b8e2e05ac48266ae02592)), closes [#903](https://github.com/vue-styleguidist/vue-styleguidist/issues/903)
* prop render needs to be view orented not react oriented ([#258](https://github.com/vue-styleguidist/vue-styleguidist/issues/258)) ([28aad2a](https://github.com/vue-styleguidist/vue-styleguidist/commit/28aad2abc1a40a27bd9dbf6fc45a735ca86f42aa))
* proper highlihght for pseudo JSX ([ed6e16f](https://github.com/vue-styleguidist/vue-styleguidist/commit/ed6e16fd2b8266194a392a2e4885554155cbdbdc))
* protect async config against parsing error ([95c6c09](https://github.com/vue-styleguidist/vue-styleguidist/commit/95c6c0969946ddb057da8d5823852d12c42f0f07))
* protect editor for null code ([8c428a4](https://github.com/vue-styleguidist/vue-styleguidist/commit/8c428a47fd11763f1762cca91996813ccb774211))
* protect pipe character in templates ([0a0befc](https://github.com/vue-styleguidist/vue-styleguidist/commit/0a0befcf85bd90fe0d7f5a31245b864aa1fcb07f))
* protect slots in if statements ([1d3d29e](https://github.com/vue-styleguidist/vue-styleguidist/commit/1d3d29e25ee31d6e2ffdc616247d29dadec6700f)), closes [#753](https://github.com/vue-styleguidist/vue-styleguidist/issues/753)
* publish templates with vsg ([f8df33f](https://github.com/vue-styleguidist/vue-styleguidist/commit/f8df33f7c50c45170b9e7bff3cb615d5263b7150))
* re-use the react hmr plugin ([2dfc5ad](https://github.com/vue-styleguidist/vue-styleguidist/commit/2dfc5adacdf195a67e14c3402ba1a20fa2742ed7))
* react-dev-utils dependency set to exactly 7.0.1 ([401afa0](https://github.com/vue-styleguidist/vue-styleguidist/commit/401afa0a77b66ca34b7c2b23e108d61b493b928f)), closes [#270](https://github.com/vue-styleguidist/vue-styleguidist/issues/270)
* register all cmpnts ins/f only first section ([4ae5390](https://github.com/vue-styleguidist/vue-styleguidist/commit/4ae5390380fd0309dee5bbd3cd99979027c93361)), closes [#405](https://github.com/vue-styleguidist/vue-styleguidist/issues/405)
* remove annoying warning about double use ([1dce586](https://github.com/vue-styleguidist/vue-styleguidist/commit/1dce586111f53975a5585da1d5f580aa06b6c425))
* remove getVars blocking imports ([1066123](https://github.com/vue-styleguidist/vue-styleguidist/commit/1066123b5405e2c6e9f486f5281b27196679fee0))
* remove invalid protection of null tags ([c609faa](https://github.com/vue-styleguidist/vue-styleguidist/commit/c609faac215bb6cfb8108db3a071513355704d8e))
* remove the propTypes error in codeSplit ([ea53a14](https://github.com/vue-styleguidist/vue-styleguidist/commit/ea53a14653311a7402cc0b23716d8b542e178797))
* remove the word "of" for sub-components ([a9f8a30](https://github.com/vue-styleguidist/vue-styleguidist/commit/a9f8a308049e15554a84d6832cbb0b5d2c6665fc))
* Remove unnecessary alias [#149](https://github.com/vue-styleguidist/vue-styleguidist/issues/149) ([8e9ce90](https://github.com/vue-styleguidist/vue-styleguidist/commit/8e9ce901760cf5484234c1be37b1fb1d8cfbb187))
* remove useless ignores ([043e4cc](https://github.com/vue-styleguidist/vue-styleguidist/commit/043e4cc96f16071d26d7321e8336552f86d0f90e))
* remove warning on arguments (description req) ([0aa01cd](https://github.com/vue-styleguidist/vue-styleguidist/commit/0aa01cda611164cd5ace1a091fe0ab93ff7861cf))
* removed webpack properties of vue-cli and example updated ([f83faf2](https://github.com/vue-styleguidist/vue-styleguidist/commit/f83faf28d3e07f4b24fd032bd853bbb45e390857))
* rename createElement ([429dd96](https://github.com/vue-styleguidist/vue-styleguidist/commit/429dd966eab92ec070f153056dd361873428b1ea))
* render default value empty string ([f41869d](https://github.com/vue-styleguidist/vue-styleguidist/commit/f41869d95684b1411850f904c2622c4424f08aa2))
* render event types as properties ([48fc5e7](https://github.com/vue-styleguidist/vue-styleguidist/commit/48fc5e70409fd67cd2ed34a3afd9e788be7aa47e))
* rendering of composed types ([86eb6bd](https://github.com/vue-styleguidist/vue-styleguidist/commit/86eb6bd8200c38bb4b7435d2ce3657aea1e35698))
* reorder aliases to allow Styleguide overrides ([9195772](https://github.com/vue-styleguidist/vue-styleguidist/commit/9195772c1514f58da646cf42423677b48125bb26))
* repair codesplit false ([c42b522](https://github.com/vue-styleguidist/vue-styleguidist/commit/c42b522b3f249a8cdf3802ade056595d20372dfa))
* repair HMR in styleguidist ([1c3e14d](https://github.com/vue-styleguidist/vue-styleguidist/commit/1c3e14d5a0966a42ba2af619558aa54836743b6c))
* requires in SFC examples ([cb22308](https://github.com/vue-styleguidist/vue-styleguidist/commit/cb223085f0e116ebb58d133a14de3216b81f5a01)), closes [#714](https://github.com/vue-styleguidist/vue-styleguidist/issues/714)
* reset styleguide changes ([01407b1](https://github.com/vue-styleguidist/vue-styleguidist/commit/01407b1d05232511888c204a627c7489f48cb50b))
* resolve [#1042](https://github.com/vue-styleguidist/vue-styleguidist/issues/1042) add basic support for aliases in external src ([6d8b5c5](https://github.com/vue-styleguidist/vue-styleguidist/commit/6d8b5c554f0cc98a3d9920292c4ace46a4cf24d8))
* resolve [#1042](https://github.com/vue-styleguidist/vue-styleguidist/issues/1042) add basic support for aliases in external src ([f4ff297](https://github.com/vue-styleguidist/vue-styleguidist/commit/f4ff2976d5cc25b8c0b7479e3028daecdd3e7cce))
* resolve conflicts ([ff45137](https://github.com/vue-styleguidist/vue-styleguidist/commit/ff45137696424526575aec9aaf118b482ff6db80))
* resolve immediately exported vars recursively ([7b27480](https://github.com/vue-styleguidist/vue-styleguidist/commit/7b27480b2954f9d9f9307d3ac717709cae3c3194))
* resolve needs to take styleguide.config into account ([#278](https://github.com/vue-styleguidist/vue-styleguidist/issues/278)) ([d5a0f78](https://github.com/vue-styleguidist/vue-styleguidist/commit/d5a0f78b50e1c2d91d994d70e10d670626c7c56e)), closes [#273](https://github.com/vue-styleguidist/vue-styleguidist/issues/273)
* Resolved dependency with new vue-loader version [#150](https://github.com/vue-styleguidist/vue-styleguidist/issues/150) ([f08398b](https://github.com/vue-styleguidist/vue-styleguidist/commit/f08398b7feff11367465df0ac10935f06a7231be))
* return app + server when command server as only app can close ([cabb746](https://github.com/vue-styleguidist/vue-styleguidist/commit/cabb746b8109b92f124cf69bfcf8fbbd48f55673))
* Revert "fix: avoid cors issue on codesandbox" ([20696ad](https://github.com/vue-styleguidist/vue-styleguidist/commit/20696ad508d97efe1e85add6704ad05154ebf359))
* Revert "fix: make library compatible with codesandbox" ([ed32d73](https://github.com/vue-styleguidist/vue-styleguidist/commit/ed32d73dbcb8894a3f5f3bcb7d6dcb7937588b13))
* run npm audit fix ([969e04c](https://github.com/vue-styleguidist/vue-styleguidist/commit/969e04c528745a54423e077329155e64892580d1))
* **safety:** update css-loader ([0b074b8](https://github.com/vue-styleguidist/vue-styleguidist/commit/0b074b84d56ff0c6b48a6d0dc85f54598ea1d208))
* section depth needs too be taken ([b663f1e](https://github.com/vue-styleguidist/vue-styleguidist/commit/b663f1eed4503b225a05c804667c4e1816c140b3))
* show - instead of undefined for untyped props ([f5a3e82](https://github.com/vue-styleguidist/vue-styleguidist/commit/f5a3e82e0d702edc8a6d57cc1db5e3e2d432cfc3)), closes [#831](https://github.com/vue-styleguidist/vue-styleguidist/issues/831)
* slot handler with vue 3 ([cec6a54](https://github.com/vue-styleguidist/vue-styleguidist/commit/cec6a54a7ee79415d75d0542bb8bd9704dfa5454))
* slot scoped parsing ([9685ba2](https://github.com/vue-styleguidist/vue-styleguidist/commit/9685ba2731795d142e1988edaa848928347dbed0))
* sort docs when all promises are resolved ([dbaa82e](https://github.com/vue-styleguidist/vue-styleguidist/commit/dbaa82e3a08113dd0182647ea2fa3a7b2b6bfdd4))
* split compiler & utils - efficient code split ([9ef9d06](https://github.com/vue-styleguidist/vue-styleguidist/commit/9ef9d06b1d541fd0733ce199b772fbbc9ba2a9fa))
* stop rendering bad event properties ([26fccd9](https://github.com/vue-styleguidist/vue-styleguidist/commit/26fccd9a78aba06f0a8403a8ff3123b8a8851aba))
* style of unlinked looked like link ([1295795](https://github.com/vue-styleguidist/vue-styleguidist/commit/1295795f9bc645026c3adf1e45b8d6c08dc74ab0)), closes [#971](https://github.com/vue-styleguidist/vue-styleguidist/issues/971)
* **styleguide:** fix scoped pseudo selectors ([#340](https://github.com/vue-styleguidist/vue-styleguidist/issues/340)) ([0e93688](https://github.com/vue-styleguidist/vue-styleguidist/commit/0e93688c34879cefc5e1caae0272081899421e1b)), closes [#335](https://github.com/vue-styleguidist/vue-styleguidist/issues/335)
* Support ie11 [#83](https://github.com/vue-styleguidist/vue-styleguidist/issues/83) ([c861735](https://github.com/vue-styleguidist/vue-styleguidist/commit/c861735a636a315ccf75cb17fd22e29d093bfc11))
* tag class for JsDoc tag values ([38fdd46](https://github.com/vue-styleguidist/vue-styleguidist/commit/38fdd462a293cbcedb6c6ef99b18217503838331))
* tag titles are no longer filtered out ([2a91b3e](https://github.com/vue-styleguidist/vue-styleguidist/commit/2a91b3e8c9aa2d1e1596cdf7998ed1af4cddf780))
* theme and styles as files ([0d33fe0](https://github.com/vue-styleguidist/vue-styleguidist/commit/0d33fe034e4ea6ea93a356b57745a8de17a32a47))
* throw when errors in promise ([7b534df](https://github.com/vue-styleguidist/vue-styleguidist/commit/7b534df46ea7ca6587e9444ba1299cdc0ecd64d5))
* transform error into warning when NOENT ([296e1cd](https://github.com/vue-styleguidist/vue-styleguidist/commit/296e1cde5614b07693241187ea559a1e2f46176f))
* transform import was not working properly ([a6df22b](https://github.com/vue-styleguidist/vue-styleguidist/commit/a6df22b5c2f9bf3e36724bd7f98c09689d2e1b9b))
* travis.yml ([3a29676](https://github.com/vue-styleguidist/vue-styleguidist/commit/3a296768991e34c89e508b1581a7b51a28b4b81a))
* types for webpack-dev-server ([205cc47](https://github.com/vue-styleguidist/vue-styleguidist/commit/205cc47febd017cacae97b9bb8b883ca7b3079ef))
* typings automated template ([711b14b](https://github.com/vue-styleguidist/vue-styleguidist/commit/711b14bffb7c733c7bdc0f75714b4ef5339dc554))
* typings of Styleguide ([f50d3b5](https://github.com/vue-styleguidist/vue-styleguidist/commit/f50d3b5b78cbab95c76f522cc2c7ae2c3b8a91c0))
* typo in documentation error ([b5ee72d](https://github.com/vue-styleguidist/vue-styleguidist/commit/b5ee72dd314c9969a84cdb2fad3ffb3d1487bbf8))
* Unknown language: "vue" ([726244f](https://github.com/vue-styleguidist/vue-styleguidist/commit/726244fc18d135fbf18bdb90b4bfad10ed3ffd98)), closes [#200](https://github.com/vue-styleguidist/vue-styleguidist/issues/200)
* update babelrc config files to the babel.config.js ([#260](https://github.com/vue-styleguidist/vue-styleguidist/issues/260)) ([0f1e28e](https://github.com/vue-styleguidist/vue-styleguidist/commit/0f1e28e27913803fbd2a9af39fd40e1bc70dc46e)), closes [webpack/webpack#8656](https://github.com/webpack/webpack/issues/8656)
* update copy-webpack-plugin to 5.1.0 ([fa2f13b](https://github.com/vue-styleguidist/vue-styleguidist/commit/fa2f13b3bd61eb6a3beed749a1ade4bc0417df71)), closes [#675](https://github.com/vue-styleguidist/vue-styleguidist/issues/675)
* update create-server for codesandbox ([4906894](https://github.com/vue-styleguidist/vue-styleguidist/commit/4906894767b9d14bb028a358bfaed69049fed91d))
* update dependencies to re-enable HMR ([860e3bc](https://github.com/vue-styleguidist/vue-styleguidist/commit/860e3bc775fd374dfd12cabdc3037e2acba811c7))
* update error management in docgen loader ([f23f267](https://github.com/vue-styleguidist/vue-styleguidist/commit/f23f267c630f9ee92742d000a4c1cfb8fe698635))
* update Prism theming & docs ([70514b9](https://github.com/vue-styleguidist/vue-styleguidist/commit/70514b95cea7d689ff404491ddf6d73c20f547a3))
* update react styleguidist to fix menu ([00ec66a](https://github.com/vue-styleguidist/vue-styleguidist/commit/00ec66ab5289b053c74fdafbf60d425ac77c2dec)), closes [#561](https://github.com/vue-styleguidist/vue-styleguidist/issues/561)
* update react styleguidist to fix menu (2) ([dba9fbf](https://github.com/vue-styleguidist/vue-styleguidist/commit/dba9fbfbdbceb24d79f0bc3b86f5dddee2cde1ca)), closes [#561](https://github.com/vue-styleguidist/vue-styleguidist/issues/561)
* update react-styleguidist ([e820c12](https://github.com/vue-styleguidist/vue-styleguidist/commit/e820c127dd835479f1517ef8b2c77d9e8771af06)), closes [#492](https://github.com/vue-styleguidist/vue-styleguidist/issues/492) [#199](https://github.com/vue-styleguidist/vue-styleguidist/issues/199)
* update react-styleguidist and merge conflicts ([ba1c341](https://github.com/vue-styleguidist/vue-styleguidist/commit/ba1c341808427d529a28abcac7a11433d2ad3389))
* update vue-docgen-api to fix the anonymous event params ([9b8f58d](https://github.com/vue-styleguidist/vue-styleguidist/commit/9b8f58d4bd444c5a52ab484939b5e6065ce286c4))
* updrade react-stypeguidist ([4823bd2](https://github.com/vue-styleguidist/vue-styleguidist/commit/4823bd2be1d1f2fa1e39d7142da7cb15ab16b43c))
* Url of Croud Style Guide on ReadMe ([5437cc4](https://github.com/vue-styleguidist/vue-styleguidist/commit/5437cc4126b9423d50c99871755bc68677202fd4))
* use globby again for kickstarting chokidar ([3e79e0d](https://github.com/vue-styleguidist/vue-styleguidist/commit/3e79e0dfe3ff4a59192ec65dc66fd1411dca814a))
* use logger instead on console ([01ac6dc](https://github.com/vue-styleguidist/vue-styleguidist/commit/01ac6dc3ad67848d51033dec1be3245f30afdba3))
* use style normalize sfc component ([fcae13c](https://github.com/vue-styleguidist/vue-styleguidist/commit/fcae13c913e1c020d3b772bd6504dd0b5985bf46))
* use the footer somewhere else than in example ([9a802c8](https://github.com/vue-styleguidist/vue-styleguidist/commit/9a802c8323ca0177c5c017b3e5946270e382406f))
* use the spread in styleguidist ([fd464a8](https://github.com/vue-styleguidist/vue-styleguidist/commit/fd464a8385b9db379b53c514e999231ed86f99fa))
* **utils:** protect docgen-cli when in SSG ([af269f6](https://github.com/vue-styleguidist/vue-styleguidist/commit/af269f6fe071d9adb67b5e11fd61e1bbc3de0963)), closes [#876](https://github.com/vue-styleguidist/vue-styleguidist/issues/876)
* **vsg:** trigger a delivery ([50670e9](https://github.com/vue-styleguidist/vue-styleguidist/commit/50670e97ce4a9d1c305fa26c463e4a7f8d2d9736))
* vue 3 resolving of dist version ([264adc3](https://github.com/vue-styleguidist/vue-styleguidist/commit/264adc38d83c6c373cf6566ae2bf67ea02335bd1)), closes [#989](https://github.com/vue-styleguidist/vue-styleguidist/issues/989)
* vuecli should work without a plugin ([#264](https://github.com/vue-styleguidist/vue-styleguidist/issues/264)) ([cd7d861](https://github.com/vue-styleguidist/vue-styleguidist/commit/cd7d8618bcd521639cd6fea735af444787ccff07)), closes [#263](https://github.com/vue-styleguidist/vue-styleguidist/issues/263)
* **vuedoc-loader:** load examples speced through an [@example](https://github.com/example) tag ([4f782aa](https://github.com/vue-styleguidist/vue-styleguidist/commit/4f782aaf3da3dc43c3bec56244065fc4dc85ef20))
* vuetype & vue-prop examples ([#262](https://github.com/vue-styleguidist/vue-styleguidist/issues/262)) ([700fff5](https://github.com/vue-styleguidist/vue-styleguidist/commit/700fff506b38284581a944783c01af7b87de4f1f))
* warning on SectionHeader ([1aaf4ee](https://github.com/vue-styleguidist/vue-styleguidist/commit/1aaf4ee4569848202f009e19529a541b9e4d0840))
* warning when documenting return of a method ([0a74e3b](https://github.com/vue-styleguidist/vue-styleguidist/commit/0a74e3bdf11fc821714ea0e0c44e008c473c9b8e))
* warning when unnamed event param ([df587dd](https://github.com/vue-styleguidist/vue-styleguidist/commit/df587dd19ac0145749020426ee134781d5af5a06))
* watch when input md changes ([54ff5ac](https://github.com/vue-styleguidist/vue-styleguidist/commit/54ff5ac3cc7b18a1a602c77ab622f5bc75cc563c))
* watcher looking at md files ([536157d](https://github.com/vue-styleguidist/vue-styleguidist/commit/536157d1c69a474d94dfa0628add7c636aa5ebe0))
* we don't need the last webpack version ([#261](https://github.com/vue-styleguidist/vue-styleguidist/issues/261)) ([9de1858](https://github.com/vue-styleguidist/vue-styleguidist/commit/9de185803341b3c78b575dc24f0091f580f3ca1f))
* webpack 4 warning ([9090878](https://github.com/vue-styleguidist/vue-styleguidist/commit/90908785a744fd9f7a47d9e502b55994448f9cf5))
* webpack dependency for yarn + storybook ([e4c5d2e](https://github.com/vue-styleguidist/vue-styleguidist/commit/e4c5d2e28091bfc2f900070e2a587c75e0fdd196))
* webpackConfig has priority on publicPath ([a06c1c6](https://github.com/vue-styleguidist/vue-styleguidist/commit/a06c1c6830203a1d0c27b59e605d2d4bc00a6ac4)), closes [#529](https://github.com/vue-styleguidist/vue-styleguidist/issues/529)
* when there is a template in the script ([35ac16d](https://github.com/vue-styleguidist/vue-styleguidist/commit/35ac16d9f3be4c172df556fe6655cc7c640cf8ae)), closes [#1000](https://github.com/vue-styleguidist/vue-styleguidist/issues/1000)
* wrong propTypes for playgroundAsync ([3fffa13](https://github.com/vue-styleguidist/vue-styleguidist/commit/3fffa13838d1a2d4939efbed787de98941a9e7a0))
* wrongly filtered and typed props array ([3495840](https://github.com/vue-styleguidist/vue-styleguidist/commit/3495840f9817d0401e8afc501ff1a71a664c1ca0))
* yarn upgrade revert ([4fe3218](https://github.com/vue-styleguidist/vue-styleguidist/commit/4fe32181053234630df418673616b5ca61321c5c))


* **docgen:** make function docgen.parse async ([e17680b](https://github.com/vue-styleguidist/vue-styleguidist/commit/e17680be53db5430ae3dd5f10d6bfdcace2ea227))
* **docgen:** make required always a boolean ([03bc88e](https://github.com/vue-styleguidist/vue-styleguidist/commit/03bc88e4606cfff918e2a9e37e2841f9576d6e50))

# [4.43.0](https://github.com/vue-styleguidist/vue-styleguidist/compare/v4.42.0...v4.43.0) (2021-11-21)

**Note:** Version bump only for package vue-styleguidist





# [4.42.0](https://github.com/vue-styleguidist/vue-styleguidist/compare/v4.41.3...v4.42.0) (2021-11-18)


### Bug Fixes

* error typing errors (so meta) ([4e4333f](https://github.com/vue-styleguidist/vue-styleguidist/commit/4e4333f5d7bb93c8bdfa73835d757bc3fe3b80dc))





## [4.41.3](https://github.com/vue-styleguidist/vue-styleguidist/compare/v4.41.2...v4.41.3) (2021-11-08)

**Note:** Version bump only for package vue-styleguidist





## [4.41.2](https://github.com/vue-styleguidist/vue-styleguidist/compare/v4.41.1...v4.41.2) (2021-09-09)

**Note:** Version bump only for package vue-styleguidist





## [4.41.1](https://github.com/vue-styleguidist/vue-styleguidist/compare/v4.41.0...v4.41.1) (2021-08-17)

**Note:** Version bump only for package vue-styleguidist





# [4.41.0](https://github.com/vue-styleguidist/vue-styleguidist/compare/v4.40.0...v4.41.0) (2021-08-13)

**Note:** Version bump only for package vue-styleguidist





# [4.40.0](https://github.com/vue-styleguidist/vue-styleguidist/compare/v4.39.0...v4.40.0) (2021-06-07)

**Note:** Version bump only for package vue-styleguidist





# [4.39.0](https://github.com/vue-styleguidist/vue-styleguidist/compare/v4.38.3...v4.39.0) (2021-05-24)

**Note:** Version bump only for package vue-styleguidist





## [4.38.3](https://github.com/vue-styleguidist/vue-styleguidist/compare/v4.38.2...v4.38.3) (2021-05-24)


### Bug Fixes

* lock dependencies as they are locked in rsg ([f418e07](https://github.com/vue-styleguidist/vue-styleguidist/commit/f418e07278a49d9717906aaf7665362effd76530))





## [4.38.2](https://github.com/vue-styleguidist/vue-styleguidist/compare/v4.38.1...v4.38.2) (2021-05-11)


### Bug Fixes

* make usage table font sizes and spacing consistent ([7fc22fe](https://github.com/vue-styleguidist/vue-styleguidist/commit/7fc22fe1d78a4f8c0c02cfdefe60feca1565254b))





## [4.38.1](https://github.com/vue-styleguidist/vue-styleguidist/compare/v4.38.0...v4.38.1) (2021-04-13)

**Note:** Version bump only for package vue-styleguidist





# [4.38.0](https://github.com/vue-styleguidist/vue-styleguidist/compare/v4.37.2...v4.38.0) (2021-04-10)

**Note:** Version bump only for package vue-styleguidist





## [4.37.1](https://github.com/vue-styleguidist/vue-styleguidist/compare/v4.37.0...v4.37.1) (2021-04-05)

**Note:** Version bump only for package vue-styleguidist





# [4.37.0](https://github.com/vue-styleguidist/vue-styleguidist/compare/v4.36.1...v4.37.0) (2021-04-05)

**Note:** Version bump only for package vue-styleguidist





## [4.36.1](https://github.com/vue-styleguidist/vue-styleguidist/compare/v4.36.0...v4.36.1) (2021-03-20)

**Note:** Version bump only for package vue-styleguidist





# [4.36.0](https://github.com/vue-styleguidist/vue-styleguidist/compare/v4.35.0...v4.36.0) (2021-03-18)


### Bug Fixes

* pagePerSection when only top section exists ([70cbb59](https://github.com/vue-styleguidist/vue-styleguidist/commit/70cbb593cbbb6f94f300c5c1ddad69c4d38e81e7)), closes [#1054](https://github.com/vue-styleguidist/vue-styleguidist/issues/1054)
* updrade react-stypeguidist ([4823bd2](https://github.com/vue-styleguidist/vue-styleguidist/commit/4823bd2be1d1f2fa1e39d7142da7cb15ab16b43c))





# [4.35.0](https://github.com/vue-styleguidist/vue-styleguidist/compare/v4.34.4...v4.35.0) (2021-01-26)

**Note:** Version bump only for package vue-styleguidist





## [4.34.2](https://github.com/vue-styleguidist/vue-styleguidist/compare/v4.34.1...v4.34.2) (2020-12-05)

**Note:** Version bump only for package vue-styleguidist





## [4.34.1](https://github.com/vue-styleguidist/vue-styleguidist/compare/v4.34.0...v4.34.1) (2020-11-27)

**Note:** Version bump only for package vue-styleguidist





# [4.34.0](https://github.com/vue-styleguidist/vue-styleguidist/compare/v4.33.10...v4.34.0) (2020-11-25)


### Features

* **cli:** render props and bindings properly ([5e4c027](https://github.com/vue-styleguidist/vue-styleguidist/commit/5e4c0272e1f5b12a7223ad52055f68871525e27f)), closes [#1013](https://github.com/vue-styleguidist/vue-styleguidist/issues/1013)





## [4.33.10](https://github.com/vue-styleguidist/vue-styleguidist/compare/v4.33.9...v4.33.10) (2020-11-20)


### Bug Fixes

* repair HMR in styleguidist ([1c3e14d](https://github.com/vue-styleguidist/vue-styleguidist/commit/1c3e14d5a0966a42ba2af619558aa54836743b6c))





## [4.33.9](https://github.com/vue-styleguidist/vue-styleguidist/compare/v4.33.8...v4.33.9) (2020-11-16)

**Note:** Version bump only for package vue-styleguidist





## [4.33.7](https://github.com/vue-styleguidist/vue-styleguidist/compare/v4.33.6...v4.33.7) (2020-11-15)

**Note:** Version bump only for package vue-styleguidist





## [4.33.6](https://github.com/vue-styleguidist/vue-styleguidist/compare/v4.33.5...v4.33.6) (2020-11-05)

**Note:** Version bump only for package vue-styleguidist





## [4.33.5](https://github.com/vue-styleguidist/vue-styleguidist/compare/v4.33.4...v4.33.5) (2020-10-23)

**Note:** Version bump only for package vue-styleguidist





## [4.33.4](https://github.com/vue-styleguidist/vue-styleguidist/compare/v4.33.3...v4.33.4) (2020-10-22)

**Note:** Version bump only for package vue-styleguidist





## [4.33.3](https://github.com/vue-styleguidist/vue-styleguidist/compare/v4.33.2...v4.33.3) (2020-10-20)


### Bug Fixes

* only display the existing API titles ([653cc18](https://github.com/vue-styleguidist/vue-styleguidist/commit/653cc181b99830c15859327ed93369eb4dfb3166))





## [4.33.2](https://github.com/vue-styleguidist/vue-styleguidist/compare/v4.33.1...v4.33.2) (2020-10-19)

**Note:** Version bump only for package vue-styleguidist





## [4.33.1](https://github.com/vue-styleguidist/vue-styleguidist/compare/v4.33.0...v4.33.1) (2020-10-14)


### Bug Fixes

* hint user when wrong version of vue installed ([b1af242](https://github.com/vue-styleguidist/vue-styleguidist/commit/b1af242ac66a721fb39859b189f9f39f597e1876))





# [4.33.0](https://github.com/vue-styleguidist/vue-styleguidist/compare/v4.32.4...v4.33.0) (2020-10-12)


### Bug Fixes

* vue 3 resolving of dist version ([264adc3](https://github.com/vue-styleguidist/vue-styleguidist/commit/264adc38d83c6c373cf6566ae2bf67ea02335bd1)), closes [#989](https://github.com/vue-styleguidist/vue-styleguidist/issues/989)





## [4.32.4](https://github.com/vue-styleguidist/vue-styleguidist/compare/v4.32.3...v4.32.4) (2020-09-24)


### Bug Fixes

* style of unlinked looked like link ([1295795](https://github.com/vue-styleguidist/vue-styleguidist/commit/1295795f9bc645026c3adf1e45b8d6c08dc74ab0)), closes [#971](https://github.com/vue-styleguidist/vue-styleguidist/issues/971)





## [4.32.3](https://github.com/vue-styleguidist/vue-styleguidist/compare/v4.32.2...v4.32.3) (2020-09-14)


### Bug Fixes

* no undefined consts ([9e28ebf](https://github.com/vue-styleguidist/vue-styleguidist/commit/9e28ebf18b4e0cea1341756d45747b1c8173af48))
* remove warning on arguments (description req) ([0aa01cd](https://github.com/vue-styleguidist/vue-styleguidist/commit/0aa01cda611164cd5ace1a091fe0ab93ff7861cf))
* warning on SectionHeader ([1aaf4ee](https://github.com/vue-styleguidist/vue-styleguidist/commit/1aaf4ee4569848202f009e19529a541b9e4d0840))





## [4.32.2](https://github.com/vue-styleguidist/vue-styleguidist/compare/v4.32.1...v4.32.2) (2020-09-10)


### Bug Fixes

* update react-styleguidist and merge conflicts ([ba1c341](https://github.com/vue-styleguidist/vue-styleguidist/commit/ba1c341808427d529a28abcac7a11433d2ad3389))





## [4.32.1](https://github.com/vue-styleguidist/vue-styleguidist/compare/v4.32.0...v4.32.1) (2020-09-08)

**Note:** Version bump only for package vue-styleguidist





# [4.32.0](https://github.com/vue-styleguidist/vue-styleguidist/compare/v4.31.2...v4.32.0) (2020-09-08)


### Bug Fixes

* use the footer somewhere else than in example ([9a802c8](https://github.com/vue-styleguidist/vue-styleguidist/commit/9a802c8323ca0177c5c017b3e5946270e382406f))


### Features

* extract footer from styleguidist for custom ([907271f](https://github.com/vue-styleguidist/vue-styleguidist/commit/907271f6f6beaf0f70c7f51ac1fe070731e74551)), closes [#935](https://github.com/vue-styleguidist/vue-styleguidist/issues/935)





## [4.31.2](https://github.com/vue-styleguidist/vue-styleguidist/compare/v4.31.1...v4.31.2) (2020-08-23)

**Note:** Version bump only for package vue-styleguidist





## [4.31.1](https://github.com/vue-styleguidist/vue-styleguidist/compare/v4.31.0...v4.31.1) (2020-08-20)


### Bug Fixes

* cypress tetss failing using previewAsync ([62c7716](https://github.com/vue-styleguidist/vue-styleguidist/commit/62c7716f2785feee2c63ba664cd0aaee5f707622))





# [4.31.0](https://github.com/vue-styleguidist/vue-styleguidist/compare/v4.30.0...v4.31.0) (2020-08-15)

**Note:** Version bump only for package vue-styleguidist





# [4.30.0](https://github.com/vue-styleguidist/vue-styleguidist/compare/v4.29.1...v4.30.0) (2020-08-08)


### Bug Fixes

* add js changes in config ([bf9880d](https://github.com/vue-styleguidist/vue-styleguidist/commit/bf9880da4292e171574a075feb838728a528b702))


### Features

* export config types to help configure ([0b44fc6](https://github.com/vue-styleguidist/vue-styleguidist/commit/0b44fc61bf6113aeb33a9b520cb3458df66b93f5))





## [4.29.1](https://github.com/vue-styleguidist/vue-styleguidist/compare/v4.29.0...v4.29.1) (2020-07-30)

**Note:** Version bump only for package vue-styleguidist





## [4.28.1](https://github.com/vue-styleguidist/vue-styleguidist/compare/v4.28.0...v4.28.1) (2020-07-27)

**Note:** Version bump only for package vue-styleguidist





# [4.28.0](https://github.com/vue-styleguidist/vue-styleguidist/compare/v4.27.1...v4.28.0) (2020-07-21)



## [4.23.3](https://github.com/vue-styleguidist/vue-styleguidist/compare/v4.23.2...v4.23.3) (2020-05-20)


### Bug Fixes

* dependency update for security ([e227276](https://github.com/vue-styleguidist/vue-styleguidist/commit/e2272766ec4469ba4fd00bb6a7e9f288e3171c75))



## [4.23.2](https://github.com/vue-styleguidist/vue-styleguidist/compare/v4.23.1...v4.23.2) (2020-05-18)


### Bug Fixes

* protect async config against parsing error ([95c6c09](https://github.com/vue-styleguidist/vue-styleguidist/commit/95c6c0969946ddb057da8d5823852d12c42f0f07))



## [4.23.1](https://github.com/vue-styleguidist/vue-styleguidist/compare/v4.23.0...v4.23.1) (2020-05-15)


### Bug Fixes

* only collapse subcomponents when > 3 ([76b4331](https://github.com/vue-styleguidist/vue-styleguidist/commit/76b43316a63d40027e2345ee855d9aee25836493))



# [4.23.0](https://github.com/vue-styleguidist/vue-styleguidist/compare/v4.22.3...v4.23.0) (2020-05-15)


### Bug Fixes

* default editor font-family ([909a47f](https://github.com/vue-styleguidist/vue-styleguidist/commit/909a47f9d888d9f1411ef63ed1b5a62f602823e6))
* issues with deprecated and requires ([efa53c9](https://github.com/vue-styleguidist/vue-styleguidist/commit/efa53c98d8fefdcc51ffeee65a99948e1f7700eb)), closes [#870](https://github.com/vue-styleguidist/vue-styleguidist/issues/870) [#871](https://github.com/vue-styleguidist/vue-styleguidist/issues/871)



## [4.22.3](https://github.com/vue-styleguidist/vue-styleguidist/compare/v4.22.2...v4.22.3) (2020-05-12)


### Bug Fixes

* throw when errors in promise ([7b534df](https://github.com/vue-styleguidist/vue-styleguidist/commit/7b534df46ea7ca6587e9444ba1299cdc0ecd64d5))



## [4.22.2](https://github.com/vue-styleguidist/vue-styleguidist/compare/v4.22.1...v4.22.2) (2020-05-12)



## [4.22.1](https://github.com/vue-styleguidist/vue-styleguidist/compare/v4.22.0...v4.22.1) (2020-05-12)


### Bug Fixes

* allow default example with custom format ([7ac7f57](https://github.com/vue-styleguidist/vue-styleguidist/commit/7ac7f57f0359be7c2b19017c6a8a4a1616c3cfd8))
* only display warning when file exists ([cfe1f9f](https://github.com/vue-styleguidist/vue-styleguidist/commit/cfe1f9faa9f109413ee7f59f611fed9a2d37579e))
* protect editor for null code ([8c428a4](https://github.com/vue-styleguidist/vue-styleguidist/commit/8c428a47fd11763f1762cca91996813ccb774211))



# [4.22.0](https://github.com/vue-styleguidist/vue-styleguidist/compare/v4.21.0...v4.22.0) (2020-05-11)


### Features

* allow for config to be an async function ([fb16a67](https://github.com/vue-styleguidist/vue-styleguidist/commit/fb16a678770162dcb37de5400259c2935566e5fb))



# [4.21.0](https://github.com/vue-styleguidist/vue-styleguidist/compare/v4.20.0...v4.21.0) (2020-05-09)



# [4.20.0](https://github.com/vue-styleguidist/vue-styleguidist/compare/v4.19.5...v4.20.0) (2020-05-06)



## [4.19.5](https://github.com/vue-styleguidist/vue-styleguidist/compare/v4.19.4...v4.19.5) (2020-05-02)



## [4.19.2](https://github.com/vue-styleguidist/vue-styleguidist/compare/v4.19.1...v4.19.2) (2020-04-29)


### Bug Fixes

* show - instead of undefined for untyped props ([f5a3e82](https://github.com/vue-styleguidist/vue-styleguidist/commit/f5a3e82e0d702edc8a6d57cc1db5e3e2d432cfc3)), closes [#831](https://github.com/vue-styleguidist/vue-styleguidist/issues/831)



## [4.19.1](https://github.com/vue-styleguidist/vue-styleguidist/compare/v4.19.0...v4.19.1) (2020-04-28)



# [4.19.0](https://github.com/vue-styleguidist/vue-styleguidist/compare/v4.18.0...v4.19.0) (2020-04-24)



# [4.18.0](https://github.com/vue-styleguidist/vue-styleguidist/compare/v4.17.0...v4.18.0) (2020-04-17)


### Bug Fixes

* make types work better ([60c2d32](https://github.com/vue-styleguidist/vue-styleguidist/commit/60c2d32981ef20f4567bd9700667bdc9df3151bb))
* rendering of composed types ([86eb6bd](https://github.com/vue-styleguidist/vue-styleguidist/commit/86eb6bd8200c38bb4b7435d2ce3657aea1e35698))
* reset styleguide changes ([01407b1](https://github.com/vue-styleguidist/vue-styleguidist/commit/01407b1d05232511888c204a627c7489f48cb50b))


### Features

* allow change padding of prism based editor ([d09b546](https://github.com/vue-styleguidist/vue-styleguidist/commit/d09b5466b3a230906da6d3c77ea177c1797abe72))
* render complex types properly ([a756455](https://github.com/vue-styleguidist/vue-styleguidist/commit/a756455bf17e751d2b4f6179e79f3fc6a2920a97))
* use pug-loader options to fuel docgen ([d2103fe](https://github.com/vue-styleguidist/vue-styleguidist/commit/d2103febadf74722945642dfefc0f4f0cdf493ac))



# [4.17.0](https://github.com/vue-styleguidist/vue-styleguidist/compare/v4.16.0...v4.17.0) (2020-04-12)


### Bug Fixes

* adapt style of sub-component ([a58f99a](https://github.com/vue-styleguidist/vue-styleguidist/commit/a58f99a4942301137b0601bb39b490243e765455))
* remove the word "of" for sub-components ([a9f8a30](https://github.com/vue-styleguidist/vue-styleguidist/commit/a9f8a308049e15554a84d6832cbb0b5d2c6665fc))


### Features

* enable indpdt HMR on sub-components ([3626933](https://github.com/vue-styleguidist/vue-styleguidist/commit/36269332f42ad3a497e763887f929dec0df24c8d))



# [4.16.0](https://github.com/vue-styleguidist/vue-styleguidist/compare/v4.15.2...v4.16.0) (2020-04-09)


### Features

* Document composite components ([#815](https://github.com/vue-styleguidist/vue-styleguidist/issues/815)) ([a6a3d11](https://github.com/vue-styleguidist/vue-styleguidist/commit/a6a3d11c320a3501ea1e63acdf3108d191cc6390)), closes [#809](https://github.com/vue-styleguidist/vue-styleguidist/issues/809)



## [4.15.2](https://github.com/vue-styleguidist/vue-styleguidist/compare/v4.15.1...v4.15.2) (2020-03-31)


### Bug Fixes

* allow ignoreWithoutExamples to work with docs ([f5bbc41](https://github.com/vue-styleguidist/vue-styleguidist/commit/f5bbc416e108db1b313d4a2c55c8e1bc845d03ca))



## [4.15.1](https://github.com/vue-styleguidist/vue-styleguidist/compare/v4.15.0...v4.15.1) (2020-03-30)


### Bug Fixes

* default example parser with alias ([361051c](https://github.com/vue-styleguidist/vue-styleguidist/commit/361051c2108cb471317721dce5917f044d446fff)), closes [#806](https://github.com/vue-styleguidist/vue-styleguidist/issues/806)
* proper highlihght for pseudo JSX ([ed6e16f](https://github.com/vue-styleguidist/vue-styleguidist/commit/ed6e16fd2b8266194a392a2e4885554155cbdbdc))



# [4.15.0](https://github.com/vue-styleguidist/vue-styleguidist/compare/v4.14.0...v4.15.0) (2020-03-29)


### Bug Fixes

* display show code when only events ([aeea160](https://github.com/vue-styleguidist/vue-styleguidist/commit/aeea1608774f87e35aafff09c41944ff6730833e)), closes [#801](https://github.com/vue-styleguidist/vue-styleguidist/issues/801)


### Features

* display [@throws](https://github.com/throws) in the JsDoc tags of methods ([8325f86](https://github.com/vue-styleguidist/vue-styleguidist/commit/8325f8698a9a68a93db4afc88f093b041a920c14)), closes [#795](https://github.com/vue-styleguidist/vue-styleguidist/issues/795)



# [4.14.0](https://github.com/vue-styleguidist/vue-styleguidist/compare/v4.13.1...v4.14.0) (2020-03-18)


### Bug Fixes

* allow webpackConfig functions to return alias ([5a6b6a3](https://github.com/vue-styleguidist/vue-styleguidist/commit/5a6b6a30ba609b2ddfbf0b6d894e802ca157f724)), closes [#793](https://github.com/vue-styleguidist/vue-styleguidist/issues/793)
* make interface for codetabbutton exported ([ccd52d8](https://github.com/vue-styleguidist/vue-styleguidist/commit/ccd52d83ed7fafa6faf2f7ad9e31ff7ef0fd375f))


### Features

* expose typescript types for theming ([3110fb5](https://github.com/vue-styleguidist/vue-styleguidist/commit/3110fb5b8342b3c89a70e9ecaf710a4c3a77bee5))



## [4.13.1](https://github.com/vue-styleguidist/vue-styleguidist/compare/v4.13.0...v4.13.1) (2020-03-03)


### Bug Fixes

* an SFC example can contain JSX ([deb2dc7](https://github.com/vue-styleguidist/vue-styleguidist/commit/deb2dc7ebda80938d59ab458faa8699f7305eb35))



# [4.13.0](https://github.com/vue-styleguidist/vue-styleguidist/compare/v4.12.2...v4.13.0) (2020-03-02)


### Bug Fixes

* avoid systematic verbose ([d43c6b0](https://github.com/vue-styleguidist/vue-styleguidist/commit/d43c6b0d8b6b5c782d806925eccd6d4b95526292))
* use logger instead on console ([01ac6dc](https://github.com/vue-styleguidist/vue-styleguidist/commit/01ac6dc3ad67848d51033dec1be3245f30afdba3))


### Features

* allow mutiple extra example files ([d06283b](https://github.com/vue-styleguidist/vue-styleguidist/commit/d06283b841bca17db6125deb83bffc3ec565ac35))



## [4.12.2](https://github.com/vue-styleguidist/vue-styleguidist/compare/v4.12.1...v4.12.2) (2020-03-02)


### Bug Fixes

* allow examples start with < & pure template ([3860129](https://github.com/vue-styleguidist/vue-styleguidist/commit/3860129c4c7bac644df39e6ac3a128a4e09ea84d))
* avoid progress bar when verbose ([75f77d0](https://github.com/vue-styleguidist/vue-styleguidist/commit/75f77d0d6f92fa2f72ec9f625ec72c3c76077c92))
* bring back the full power of verbose option ([210bae2](https://github.com/vue-styleguidist/vue-styleguidist/commit/210bae2c9e5b935c17cb7add5bfdef9459b90a6c))
* hot reload default examples ([295cfe5](https://github.com/vue-styleguidist/vue-styleguidist/commit/295cfe553ce8d5e48a3c7607672017246d65d992))



## [4.12.1](https://github.com/vue-styleguidist/vue-styleguidist/compare/v4.12.0...v4.12.1) (2020-02-26)


### Bug Fixes

* remove annoying warning about double use ([1dce586](https://github.com/vue-styleguidist/vue-styleguidist/commit/1dce586111f53975a5585da1d5f580aa06b6c425))



# [4.12.0](https://github.com/vue-styleguidist/vue-styleguidist/compare/v4.11.0...v4.12.0) (2020-02-25)


### Bug Fixes

* pre classname warning ([b80f97a](https://github.com/vue-styleguidist/vue-styleguidist/commit/b80f97a9818410b46ec03552d27b2b59b18531d8))


### Features

* allow to ignore some example file lookup ([7104271](https://github.com/vue-styleguidist/vue-styleguidist/commit/71042712b28afc519ad631f873d8e62a87b821ae))
* detect when example file loaded twice ([e4b1a48](https://github.com/vue-styleguidist/vue-styleguidist/commit/e4b1a4808f0b175bb0a23088e139595da58b14c4))



# [4.11.0](https://github.com/vue-styleguidist/vue-styleguidist/compare/v4.10.1...v4.11.0) (2020-02-22)


### Features

* give default examples a variable geometry ([535e347](https://github.com/vue-styleguidist/vue-styleguidist/commit/535e347e3970b5c48a40ac538892cffe85a89977))



## [4.10.1](https://github.com/vue-styleguidist/vue-styleguidist/compare/v4.10.0...v4.10.1) (2020-02-17)


### Bug Fixes

* update Prism theming & docs ([70514b9](https://github.com/vue-styleguidist/vue-styleguidist/commit/70514b95cea7d689ff404491ddf6d73c20f547a3))



# [4.10.0](https://github.com/vue-styleguidist/vue-styleguidist/compare/v4.9.1...v4.10.0) (2020-02-17)


### Features

* allow usage of prism themes ([921dbd5](https://github.com/vue-styleguidist/vue-styleguidist/commit/921dbd5e26c420d692f607a7f18bcff4e626d404))


### Reverts

* Revert "refactor: avoid converting events and slots" ([d8e4d4d](https://github.com/vue-styleguidist/vue-styleguidist/commit/d8e4d4dfab49c26be1ca2e254144b57c69a3019e))



## [4.9.1](https://github.com/vue-styleguidist/vue-styleguidist/compare/v4.9.0...v4.9.1) (2020-02-16)


### Bug Fixes

* create-server bis for csb ([0768c51](https://github.com/vue-styleguidist/vue-styleguidist/commit/0768c5143d41762caf59f5d07a1e0bc74a2570e3))
* update create-server for codesandbox ([4906894](https://github.com/vue-styleguidist/vue-styleguidist/commit/4906894767b9d14bb028a358bfaed69049fed91d))



# [4.9.0](https://github.com/vue-styleguidist/vue-styleguidist/compare/v4.8.1...v4.9.0) (2020-02-16)


### Bug Fixes

* make origin column smaller ([4b7027e](https://github.com/vue-styleguidist/vue-styleguidist/commit/4b7027ec27a03c8be588d93c3d2e8772e07d45d9))





## [4.27.1](https://github.com/vue-styleguidist/vue-styleguidist/compare/v4.27.0...v4.27.1) (2020-07-19)

**Note:** Version bump only for package vue-styleguidist





# [4.27.0](https://github.com/vue-styleguidist/vue-styleguidist/compare/v4.26.3...v4.27.0) (2020-07-17)

**Note:** Version bump only for package vue-styleguidist





# [4.26.0](https://github.com/vue-styleguidist/vue-styleguidist/compare/v4.25.0...v4.26.0) (2020-06-29)


### Bug Fixes

* compile issue linked to progress bar fix ([5c55eaf](https://github.com/vue-styleguidist/vue-styleguidist/commit/5c55eaf5927bbb3e9479a916f9c59cab6416a56c))
* progressbar assumed custom webpack ([96e28e6](https://github.com/vue-styleguidist/vue-styleguidist/commit/96e28e64ff139922ce5b8e2e05ac48266ae02592)), closes [#903](https://github.com/vue-styleguidist/vue-styleguidist/issues/903)





# [4.25.0](https://github.com/vue-styleguidist/vue-styleguidist/compare/v4.24.3...v4.25.0) (2020-06-19)


### Bug Fixes

* yarn upgrade revert ([4fe3218](https://github.com/vue-styleguidist/vue-styleguidist/commit/4fe32181053234630df418673616b5ca61321c5c))


### Features

* 🎸 Add support for html language in examples ([77e225a](https://github.com/vue-styleguidist/vue-styleguidist/commit/77e225a3afcda1dabe87d7e52042748e48799d6b))





## [4.24.3](https://github.com/vue-styleguidist/vue-styleguidist/compare/v4.24.2...v4.24.3) (2020-06-16)

**Note:** Version bump only for package vue-styleguidist





## [4.24.1](https://github.com/vue-styleguidist/vue-styleguidist/compare/v4.24.0...v4.24.1) (2020-06-05)


### Bug Fixes

* repair codesplit false ([c42b522](https://github.com/vue-styleguidist/vue-styleguidist/commit/c42b522b3f249a8cdf3802ade056595d20372dfa))





# [4.24.0](https://github.com/vue-styleguidist/vue-styleguidist/compare/v4.23.3...v4.24.0) (2020-05-28)


### Bug Fixes

* adjust structure of examples ([b80a86c](https://github.com/vue-styleguidist/vue-styleguidist/commit/b80a86c3aad769530cdab4ea911639088a2b8dac))
* classname and style were ignored ([563b313](https://github.com/vue-styleguidist/vue-styleguidist/commit/563b3134ee1304790d06b2f4d394cc79400be9da))





## [4.23.3](https://github.com/vue-styleguidist/vue-styleguidist/compare/v4.23.2...v4.23.3) (2020-05-20)


### Bug Fixes

* dependency update for security ([e227276](https://github.com/vue-styleguidist/vue-styleguidist/commit/e2272766ec4469ba4fd00bb6a7e9f288e3171c75))





## [4.23.2](https://github.com/vue-styleguidist/vue-styleguidist/compare/v4.23.1...v4.23.2) (2020-05-18)


### Bug Fixes

* protect async config against parsing error ([95c6c09](https://github.com/vue-styleguidist/vue-styleguidist/commit/95c6c0969946ddb057da8d5823852d12c42f0f07))





## [4.23.1](https://github.com/vue-styleguidist/vue-styleguidist/compare/v4.23.0...v4.23.1) (2020-05-15)


### Bug Fixes

* only collapse subcomponents when > 3 ([76b4331](https://github.com/vue-styleguidist/vue-styleguidist/commit/76b43316a63d40027e2345ee855d9aee25836493))





# [4.23.0](https://github.com/vue-styleguidist/vue-styleguidist/compare/v4.22.3...v4.23.0) (2020-05-15)


### Bug Fixes

* default editor font-family ([909a47f](https://github.com/vue-styleguidist/vue-styleguidist/commit/909a47f9d888d9f1411ef63ed1b5a62f602823e6))
* issues with deprecated and requires ([efa53c9](https://github.com/vue-styleguidist/vue-styleguidist/commit/efa53c98d8fefdcc51ffeee65a99948e1f7700eb)), closes [#870](https://github.com/vue-styleguidist/vue-styleguidist/issues/870) [#871](https://github.com/vue-styleguidist/vue-styleguidist/issues/871)





## [4.22.3](https://github.com/vue-styleguidist/vue-styleguidist/compare/v4.22.2...v4.22.3) (2020-05-12)


### Bug Fixes

* throw when errors in promise ([7b534df](https://github.com/vue-styleguidist/vue-styleguidist/commit/7b534df46ea7ca6587e9444ba1299cdc0ecd64d5))





## [4.22.2](https://github.com/vue-styleguidist/vue-styleguidist/compare/v4.22.1...v4.22.2) (2020-05-12)

**Note:** Version bump only for package vue-styleguidist





## [4.22.1](https://github.com/vue-styleguidist/vue-styleguidist/compare/v4.22.0...v4.22.1) (2020-05-12)


### Bug Fixes

* allow default example with custom format ([7ac7f57](https://github.com/vue-styleguidist/vue-styleguidist/commit/7ac7f57f0359be7c2b19017c6a8a4a1616c3cfd8))
* only display warning when file exists ([cfe1f9f](https://github.com/vue-styleguidist/vue-styleguidist/commit/cfe1f9faa9f109413ee7f59f611fed9a2d37579e))
* protect editor for null code ([8c428a4](https://github.com/vue-styleguidist/vue-styleguidist/commit/8c428a47fd11763f1762cca91996813ccb774211))





# [4.22.0](https://github.com/vue-styleguidist/vue-styleguidist/compare/v4.21.0...v4.22.0) (2020-05-11)


### Features

* allow for config to be an async function ([fb16a67](https://github.com/vue-styleguidist/vue-styleguidist/commit/fb16a678770162dcb37de5400259c2935566e5fb))





# [4.21.0](https://github.com/vue-styleguidist/vue-styleguidist/compare/v4.20.0...v4.21.0) (2020-05-09)

**Note:** Version bump only for package vue-styleguidist





# [4.20.0](https://github.com/vue-styleguidist/vue-styleguidist/compare/v4.19.5...v4.20.0) (2020-05-06)

**Note:** Version bump only for package vue-styleguidist





## [4.19.5](https://github.com/vue-styleguidist/vue-styleguidist/compare/v4.19.4...v4.19.5) (2020-05-02)

**Note:** Version bump only for package vue-styleguidist





## [4.19.2](https://github.com/vue-styleguidist/vue-styleguidist/compare/v4.19.1...v4.19.2) (2020-04-29)


### Bug Fixes

* show - instead of undefined for untyped props ([f5a3e82](https://github.com/vue-styleguidist/vue-styleguidist/commit/f5a3e82e0d702edc8a6d57cc1db5e3e2d432cfc3)), closes [#831](https://github.com/vue-styleguidist/vue-styleguidist/issues/831)





## [4.19.1](https://github.com/vue-styleguidist/vue-styleguidist/compare/v4.19.0...v4.19.1) (2020-04-28)

**Note:** Version bump only for package vue-styleguidist





# [4.19.0](https://github.com/vue-styleguidist/vue-styleguidist/compare/v4.18.0...v4.19.0) (2020-04-24)

**Note:** Version bump only for package vue-styleguidist





# [4.18.0](https://github.com/vue-styleguidist/vue-styleguidist/compare/v4.17.0...v4.18.0) (2020-04-17)


### Bug Fixes

* rendering of composed types ([86eb6bd](https://github.com/vue-styleguidist/vue-styleguidist/commit/86eb6bd8200c38bb4b7435d2ce3657aea1e35698))


### Features

* allow change padding of prism based editor ([d09b546](https://github.com/vue-styleguidist/vue-styleguidist/commit/d09b5466b3a230906da6d3c77ea177c1797abe72))
* render complex types properly ([a756455](https://github.com/vue-styleguidist/vue-styleguidist/commit/a756455bf17e751d2b4f6179e79f3fc6a2920a97))





# [4.17.0](https://github.com/vue-styleguidist/vue-styleguidist/compare/v4.16.0...v4.17.0) (2020-04-12)


### Bug Fixes

* adapt style of sub-component ([a58f99a](https://github.com/vue-styleguidist/vue-styleguidist/commit/a58f99a4942301137b0601bb39b490243e765455))
* remove the word "of" for sub-components ([a9f8a30](https://github.com/vue-styleguidist/vue-styleguidist/commit/a9f8a308049e15554a84d6832cbb0b5d2c6665fc))


### Features

* enable indpdt HMR on sub-components ([3626933](https://github.com/vue-styleguidist/vue-styleguidist/commit/36269332f42ad3a497e763887f929dec0df24c8d))





# [4.16.0](https://github.com/vue-styleguidist/vue-styleguidist/compare/v4.15.2...v4.16.0) (2020-04-09)


### Features

* Document composite components ([#815](https://github.com/vue-styleguidist/vue-styleguidist/issues/815)) ([a6a3d11](https://github.com/vue-styleguidist/vue-styleguidist/commit/a6a3d11c320a3501ea1e63acdf3108d191cc6390)), closes [#809](https://github.com/vue-styleguidist/vue-styleguidist/issues/809)





## [4.15.2](https://github.com/vue-styleguidist/vue-styleguidist/compare/v4.15.1...v4.15.2) (2020-03-31)


### Bug Fixes

* allow ignoreWithoutExamples to work with docs ([f5bbc41](https://github.com/vue-styleguidist/vue-styleguidist/commit/f5bbc416e108db1b313d4a2c55c8e1bc845d03ca))





## [4.15.1](https://github.com/vue-styleguidist/vue-styleguidist/compare/v4.15.0...v4.15.1) (2020-03-30)


### Bug Fixes

* default example parser with alias ([361051c](https://github.com/vue-styleguidist/vue-styleguidist/commit/361051c2108cb471317721dce5917f044d446fff)), closes [#806](https://github.com/vue-styleguidist/vue-styleguidist/issues/806)
* proper highlihght for pseudo JSX ([ed6e16f](https://github.com/vue-styleguidist/vue-styleguidist/commit/ed6e16fd2b8266194a392a2e4885554155cbdbdc))





# [4.15.0](https://github.com/vue-styleguidist/vue-styleguidist/compare/v4.14.0...v4.15.0) (2020-03-29)


### Bug Fixes

* display show code when only events ([aeea160](https://github.com/vue-styleguidist/vue-styleguidist/commit/aeea1608774f87e35aafff09c41944ff6730833e)), closes [#801](https://github.com/vue-styleguidist/vue-styleguidist/issues/801)


### Features

* display [@throws](https://github.com/throws) in the JsDoc tags of methods ([8325f86](https://github.com/vue-styleguidist/vue-styleguidist/commit/8325f8698a9a68a93db4afc88f093b041a920c14)), closes [#795](https://github.com/vue-styleguidist/vue-styleguidist/issues/795)





# [4.14.0](https://github.com/vue-styleguidist/vue-styleguidist/compare/v4.13.1...v4.14.0) (2020-03-18)


### Bug Fixes

* allow webpackConfig functions to return alias ([5a6b6a3](https://github.com/vue-styleguidist/vue-styleguidist/commit/5a6b6a30ba609b2ddfbf0b6d894e802ca157f724)), closes [#793](https://github.com/vue-styleguidist/vue-styleguidist/issues/793)
* make interface for codetabbutton exported ([ccd52d8](https://github.com/vue-styleguidist/vue-styleguidist/commit/ccd52d83ed7fafa6faf2f7ad9e31ff7ef0fd375f))


### Features

* expose typescript types for theming ([3110fb5](https://github.com/vue-styleguidist/vue-styleguidist/commit/3110fb5b8342b3c89a70e9ecaf710a4c3a77bee5))





## [4.13.1](https://github.com/vue-styleguidist/vue-styleguidist/compare/v4.13.0...v4.13.1) (2020-03-03)


### Bug Fixes

* an SFC example can contain JSX ([deb2dc7](https://github.com/vue-styleguidist/vue-styleguidist/commit/deb2dc7ebda80938d59ab458faa8699f7305eb35))





# [4.13.0](https://github.com/vue-styleguidist/vue-styleguidist/compare/v4.12.2...v4.13.0) (2020-03-02)


### Bug Fixes

* avoid systematic verbose ([d43c6b0](https://github.com/vue-styleguidist/vue-styleguidist/commit/d43c6b0d8b6b5c782d806925eccd6d4b95526292))
* use logger instead on console ([01ac6dc](https://github.com/vue-styleguidist/vue-styleguidist/commit/01ac6dc3ad67848d51033dec1be3245f30afdba3))


### Features

* allow mutiple extra example files ([d06283b](https://github.com/vue-styleguidist/vue-styleguidist/commit/d06283b841bca17db6125deb83bffc3ec565ac35))





## [4.12.2](https://github.com/vue-styleguidist/vue-styleguidist/compare/v4.12.1...v4.12.2) (2020-03-02)


### Bug Fixes

* allow examples start with < & pure template ([3860129](https://github.com/vue-styleguidist/vue-styleguidist/commit/3860129c4c7bac644df39e6ac3a128a4e09ea84d))
* avoid progress bar when verbose ([75f77d0](https://github.com/vue-styleguidist/vue-styleguidist/commit/75f77d0d6f92fa2f72ec9f625ec72c3c76077c92))
* bring back the full power of verbose option ([210bae2](https://github.com/vue-styleguidist/vue-styleguidist/commit/210bae2c9e5b935c17cb7add5bfdef9459b90a6c))
* hot reload default examples ([295cfe5](https://github.com/vue-styleguidist/vue-styleguidist/commit/295cfe553ce8d5e48a3c7607672017246d65d992))





## [4.12.1](https://github.com/vue-styleguidist/vue-styleguidist/compare/v4.12.0...v4.12.1) (2020-02-26)


### Bug Fixes

* remove annoying warning about double use ([1dce586](https://github.com/vue-styleguidist/vue-styleguidist/commit/1dce586111f53975a5585da1d5f580aa06b6c425))





# [4.12.0](https://github.com/vue-styleguidist/vue-styleguidist/compare/v4.11.0...v4.12.0) (2020-02-25)


### Bug Fixes

* pre classname warning ([b80f97a](https://github.com/vue-styleguidist/vue-styleguidist/commit/b80f97a9818410b46ec03552d27b2b59b18531d8))


### Features

* allow to ignore some example file lookup ([7104271](https://github.com/vue-styleguidist/vue-styleguidist/commit/71042712b28afc519ad631f873d8e62a87b821ae))
* detect when example file loaded twice ([e4b1a48](https://github.com/vue-styleguidist/vue-styleguidist/commit/e4b1a4808f0b175bb0a23088e139595da58b14c4))





# [4.11.0](https://github.com/vue-styleguidist/vue-styleguidist/compare/v4.10.1...v4.11.0) (2020-02-22)


### Features

* give default examples a variable geometry ([535e347](https://github.com/vue-styleguidist/vue-styleguidist/commit/535e347e3970b5c48a40ac538892cffe85a89977))





## [4.10.1](https://github.com/vue-styleguidist/vue-styleguidist/compare/v4.10.0...v4.10.1) (2020-02-17)


### Bug Fixes

* update Prism theming & docs ([70514b9](https://github.com/vue-styleguidist/vue-styleguidist/commit/70514b95cea7d689ff404491ddf6d73c20f547a3))





# [4.10.0](https://github.com/vue-styleguidist/vue-styleguidist/compare/v4.9.1...v4.10.0) (2020-02-17)


### Features

* allow usage of prism themes ([921dbd5](https://github.com/vue-styleguidist/vue-styleguidist/commit/921dbd5e26c420d692f607a7f18bcff4e626d404))


### Reverts

* Revert "refactor: avoid converting events and slots" ([d8e4d4d](https://github.com/vue-styleguidist/vue-styleguidist/commit/d8e4d4dfab49c26be1ca2e254144b57c69a3019e))





## [4.9.1](https://github.com/vue-styleguidist/vue-styleguidist/compare/v4.9.0...v4.9.1) (2020-02-16)


### Bug Fixes

* create-server bis for csb ([0768c51](https://github.com/vue-styleguidist/vue-styleguidist/commit/0768c5143d41762caf59f5d07a1e0bc74a2570e3))
* update create-server for codesandbox ([4906894](https://github.com/vue-styleguidist/vue-styleguidist/commit/4906894767b9d14bb028a358bfaed69049fed91d))





# [4.9.0](https://github.com/vue-styleguidist/vue-styleguidist/compare/v4.8.1...v4.9.0) (2020-02-16)


### Bug Fixes

* make origin column smaller ([4b7027e](https://github.com/vue-styleguidist/vue-styleguidist/commit/4b7027ec27a03c8be588d93c3d2e8772e07d45d9))
* stop rendering bad event properties ([26fccd9](https://github.com/vue-styleguidist/vue-styleguidist/commit/26fccd9a78aba06f0a8403a8ff3123b8a8851aba))


### Features

* origin column on props event methods & slots ([8b0650f](https://github.com/vue-styleguidist/vue-styleguidist/commit/8b0650f08d3c4cde0970fd87aabb439cd1e06ef0))





## [4.8.1](https://github.com/vue-styleguidist/vue-styleguidist/compare/v4.8.0...v4.8.1) (2020-02-13)

**Note:** Version bump only for package vue-styleguidist





# [4.8.0](https://github.com/vue-styleguidist/vue-styleguidist/compare/v4.7.7...v4.8.0) (2020-02-12)

**Note:** Version bump only for package vue-styleguidist





## [4.7.7](https://github.com/vue-styleguidist/vue-styleguidist/compare/v4.7.6...v4.7.7) (2020-02-10)

**Note:** Version bump only for package vue-styleguidist





## [4.7.6](https://github.com/vue-styleguidist/vue-styleguidist/compare/v4.7.5...v4.7.6) (2020-01-23)

**Note:** Version bump only for package vue-styleguidist





## [4.7.5](https://github.com/vue-styleguidist/vue-styleguidist/compare/v4.7.4...v4.7.5) (2020-01-23)

**Note:** Version bump only for package vue-styleguidist





## [4.7.4](https://github.com/vue-styleguidist/vue-styleguidist/compare/v4.7.3...v4.7.4) (2020-01-22)

**Note:** Version bump only for package vue-styleguidist





## [4.7.3](https://github.com/vue-styleguidist/vue-styleguidist/compare/v4.7.2...v4.7.3) (2020-01-21)


### Bug Fixes

* Revert "fix: avoid cors issue on codesandbox" ([20696ad](https://github.com/vue-styleguidist/vue-styleguidist/commit/20696ad508d97efe1e85add6704ad05154ebf359))
* Revert "fix: make library compatible with codesandbox" ([ed32d73](https://github.com/vue-styleguidist/vue-styleguidist/commit/ed32d73dbcb8894a3f5f3bcb7d6dcb7937588b13))





## [4.7.2](https://github.com/vue-styleguidist/vue-styleguidist/compare/v4.7.1...v4.7.2) (2020-01-20)


### Bug Fixes

* avoid cors issue on codesandbox ([26450b2](https://github.com/vue-styleguidist/vue-styleguidist/commit/26450b28535eeb032385a99799a05f6ccf1f7951))





## [4.7.1](https://github.com/vue-styleguidist/vue-styleguidist/compare/v4.7.0...v4.7.1) (2020-01-20)


### Bug Fixes

* make library compatible with codesandbox ([c4b531d](https://github.com/vue-styleguidist/vue-styleguidist/commit/c4b531db8d00f31eb9aad6aa240cb3ab1415541e))





# [4.7.0](https://github.com/vue-styleguidist/vue-styleguidist/compare/v4.6.1...v4.7.0) (2020-01-20)

**Note:** Version bump only for package vue-styleguidist





## [4.6.1](https://github.com/vue-styleguidist/vue-styleguidist/compare/v4.6.0...v4.6.1) (2020-01-19)


### Bug Fixes

* call to  ast-types builder ([071b067](https://github.com/vue-styleguidist/vue-styleguidist/commit/071b06717b9d93c125e71ec07e054a3a9811c58f))
* theme and styles as files ([0d33fe0](https://github.com/vue-styleguidist/vue-styleguidist/commit/0d33fe034e4ea6ea93a356b57745a8de17a32a47))





# [4.6.0](https://github.com/vue-styleguidist/vue-styleguidist/compare/v4.5.2...v4.6.0) (2020-01-19)

**Note:** Version bump only for package vue-styleguidist





## [4.5.2](https://github.com/vue-styleguidist/vue-styleguidist/compare/v4.5.1...v4.5.2) (2020-01-17)

**Note:** Version bump only for package vue-styleguidist





## [4.5.1](https://github.com/vue-styleguidist/vue-styleguidist/compare/v4.5.0...v4.5.1) (2020-01-16)


### Bug Fixes

* requires in SFC examples ([cb22308](https://github.com/vue-styleguidist/vue-styleguidist/commit/cb223085f0e116ebb58d133a14de3216b81f5a01)), closes [#714](https://github.com/vue-styleguidist/vue-styleguidist/issues/714)





# [4.5.0](https://github.com/vue-styleguidist/vue-styleguidist/compare/v4.4.3...v4.5.0) (2020-01-15)


### Bug Fixes

* typings automated template ([711b14b](https://github.com/vue-styleguidist/vue-styleguidist/commit/711b14bffb7c733c7bdc0f75714b4ef5339dc554))


### Features

* update rsg with new theming ([af0ceb2](https://github.com/vue-styleguidist/vue-styleguidist/commit/af0ceb2bba6f9b8fe6a000c121e02b7a2e435c8c))





## [4.4.3](https://github.com/vue-styleguidist/vue-styleguidist/compare/v4.4.2...v4.4.3) (2020-01-11)


### Bug Fixes

* new Vue compatible with live require ([f3f68a8](https://github.com/vue-styleguidist/vue-styleguidist/commit/f3f68a83651c12d0293a395cc7f997c1eb2e8f36)), closes [#702](https://github.com/vue-styleguidist/vue-styleguidist/issues/702)





## [4.4.2](https://github.com/vue-styleguidist/vue-styleguidist/compare/v4.4.1...v4.4.2) (2020-01-10)

**Note:** Version bump only for package vue-styleguidist





## [4.4.1](https://github.com/vue-styleguidist/vue-styleguidist/compare/v4.4.0...v4.4.1) (2020-01-09)


### Bug Fixes

* collapsible sections tocMode ([e5f7bfd](https://github.com/vue-styleguidist/vue-styleguidist/commit/e5f7bfdfc70acaa97ed7ae297363c418dfec3001))





# [4.4.0](https://github.com/vue-styleguidist/vue-styleguidist/compare/v4.3.0...v4.4.0) (2020-01-09)


### Bug Fixes

* update error management in docgen loader ([f23f267](https://github.com/vue-styleguidist/vue-styleguidist/commit/f23f267c630f9ee92742d000a4c1cfb8fe698635))
* warning when documenting return of a method ([0a74e3b](https://github.com/vue-styleguidist/vue-styleguidist/commit/0a74e3bdf11fc821714ea0e0c44e008c473c9b8e))





# [4.3.0](https://github.com/vue-styleguidist/vue-styleguidist/compare/v4.2.3...v4.3.0) (2020-01-08)


### Features

* collapsible sections ([43715a7](https://github.com/vue-styleguidist/vue-styleguidist/commit/43715a7532d07ba5f5f868a2628fbc10eae52543)), closes [#689](https://github.com/vue-styleguidist/vue-styleguidist/issues/689)





## [4.2.3](https://github.com/vue-styleguidist/vue-styleguidist/compare/v4.2.2...v4.2.3) (2019-12-23)


### Bug Fixes

* add warning when using editorConfig ([b39f6f8](https://github.com/vue-styleguidist/vue-styleguidist/commit/b39f6f851455f208a9ae9092c8226cf2f7c3322c))
* typings of Styleguide ([f50d3b5](https://github.com/vue-styleguidist/vue-styleguidist/commit/f50d3b5b78cbab95c76f522cc2c7ae2c3b8a91c0))





## [4.2.2](https://github.com/vue-styleguidist/vue-styleguidist/compare/v4.2.1...v4.2.2) (2019-12-18)

**Note:** Version bump only for package vue-styleguidist





## [4.2.1](https://github.com/vue-styleguidist/vue-styleguidist/compare/v4.2.0...v4.2.1) (2019-12-11)


### Bug Fixes

* update copy-webpack-plugin to 5.1.0 ([fa2f13b](https://github.com/vue-styleguidist/vue-styleguidist/commit/fa2f13b)), closes [#675](https://github.com/vue-styleguidist/vue-styleguidist/issues/675)





# [4.2.0](https://github.com/vue-styleguidist/vue-styleguidist/compare/v4.1.2...v4.2.0) (2019-12-10)


### Features

* pass validExtends to styleguide.config.js ([c22f7d5](https://github.com/vue-styleguidist/vue-styleguidist/commit/c22f7d5))





## [4.1.2](https://github.com/vue-styleguidist/vue-styleguidist/compare/v4.1.1...v4.1.2) (2019-12-08)

**Note:** Version bump only for package vue-styleguidist





## [4.1.1](https://github.com/vue-styleguidist/vue-styleguidist/compare/v4.1.0...v4.1.1) (2019-12-05)

**Note:** Version bump only for package vue-styleguidist





# [4.1.0](https://github.com/vue-styleguidist/vue-styleguidist/compare/v4.0.8...v4.1.0) (2019-12-04)

**Note:** Version bump only for package vue-styleguidist





## [4.0.8](https://github.com/vue-styleguidist/vue-styleguidist/compare/v4.0.7...v4.0.8) (2019-12-02)


### Bug Fixes

* make sections without examples pre-compile ([56d675d](https://github.com/vue-styleguidist/vue-styleguidist/commit/56d675d))





## [4.0.7](https://github.com/vue-styleguidist/vue-styleguidist/compare/v4.0.6...v4.0.7) (2019-12-01)


### Bug Fixes

* destroy Vue component in Preview when replacing it or unmounting ([00b7658](https://github.com/vue-styleguidist/vue-styleguidist/commit/00b7658))





## [4.0.5](https://github.com/vue-styleguidist/vue-styleguidist/compare/v4.0.4...v4.0.5) (2019-11-20)

**Note:** Version bump only for package vue-styleguidist





## [4.0.4](https://github.com/vue-styleguidist/vue-styleguidist/compare/v4.0.3...v4.0.4) (2019-11-19)

**Note:** Version bump only for package vue-styleguidist





## [4.0.3](https://github.com/vue-styleguidist/vue-styleguidist/compare/v4.0.2...v4.0.3) (2019-11-19)


### Bug Fixes

* babel typescript snafu ([d72c43e](https://github.com/vue-styleguidist/vue-styleguidist/commit/d72c43e)), closes [#639](https://github.com/vue-styleguidist/vue-styleguidist/issues/639)





## [4.0.2](https://github.com/vue-styleguidist/vue-styleguidist/compare/v4.0.1...v4.0.2) (2019-11-18)


### Bug Fixes

* import issue conflicts with babel ([f1ac618](https://github.com/vue-styleguidist/vue-styleguidist/commit/f1ac618)), closes [#635](https://github.com/vue-styleguidist/vue-styleguidist/issues/635)





## [4.0.1](https://github.com/vue-styleguidist/vue-styleguidist/compare/v4.0.0...v4.0.1) (2019-11-15)

**Note:** Version bump only for package vue-styleguidist





# [4.0.0-beta.20](https://github.com/vue-styleguidist/vue-styleguidist/compare/v4.0.0-beta.19...v4.0.0-beta.20) (2019-11-15)

**Note:** Version bump only for package vue-styleguidist





# [4.0.0-beta.19](https://github.com/vue-styleguidist/vue-styleguidist/compare/v4.0.0-beta.18...v4.0.0-beta.19) (2019-11-14)


### Bug Fixes

* mixed scoped and non-scoped slots render ([4161ff2](https://github.com/vue-styleguidist/vue-styleguidist/commit/4161ff2))


### Features

* review the style of default functions ([98ae04c](https://github.com/vue-styleguidist/vue-styleguidist/commit/98ae04c))





# [4.0.0-beta.18](https://github.com/vue-styleguidist/vue-styleguidist/compare/v4.0.0-beta.17...v4.0.0-beta.18) (2019-11-14)


### Features

* make arrow functions default cleaner ([f16b424](https://github.com/vue-styleguidist/vue-styleguidist/commit/f16b424))





# [4.0.0-beta.17](https://github.com/vue-styleguidist/vue-styleguidist/compare/v4.0.0-beta.16...v4.0.0-beta.17) (2019-11-14)


### Features

* use bindings comments in styleguidist ([4fb6551](https://github.com/vue-styleguidist/vue-styleguidist/commit/4fb6551))





# [4.0.0-beta.16](https://github.com/vue-styleguidist/vue-styleguidist/compare/v4.0.0-beta.15...v4.0.0-beta.16) (2019-11-13)


### Bug Fixes

* warning when unnamed event param ([df587dd](https://github.com/vue-styleguidist/vue-styleguidist/commit/df587dd))





# [4.0.0-beta.15](https://github.com/vue-styleguidist/vue-styleguidist/compare/v4.0.0-beta.14...v4.0.0-beta.15) (2019-11-13)


### Bug Fixes

* render event types as properties ([48fc5e7](https://github.com/vue-styleguidist/vue-styleguidist/commit/48fc5e7))


### Features

* review design of all props output ([cc80bd5](https://github.com/vue-styleguidist/vue-styleguidist/commit/cc80bd5))
* use [@values](https://github.com/values) tag in props ([cb2fc74](https://github.com/vue-styleguidist/vue-styleguidist/commit/cb2fc74)), closes [#345](https://github.com/vue-styleguidist/vue-styleguidist/issues/345)





# [4.0.0-beta.14](https://github.com/vue-styleguidist/vue-styleguidist/compare/v4.0.0-beta.13...v4.0.0-beta.14) (2019-11-10)


### Bug Fixes

* section depth needs too be taken ([b663f1e](https://github.com/vue-styleguidist/vue-styleguidist/commit/b663f1e))
* wrongly filtered and typed props array ([3495840](https://github.com/vue-styleguidist/vue-styleguidist/commit/3495840))


### Features

* add option to disable progress bar ([6ec4e9d](https://github.com/vue-styleguidist/vue-styleguidist/commit/6ec4e9d))





# [4.0.0-beta.13](https://github.com/vue-styleguidist/vue-styleguidist/compare/v4.0.0-beta.12...v4.0.0-beta.13) (2019-11-06)


### Bug Fixes

* avoid double progressBar ([e39878e](https://github.com/vue-styleguidist/vue-styleguidist/commit/e39878e))





# [4.0.0-beta.12](https://github.com/vue-styleguidist/vue-styleguidist/compare/v4.0.0-beta.11...v4.0.0-beta.12) (2019-11-06)

**Note:** Version bump only for package vue-styleguidist





# [4.0.0-beta.11](https://github.com/vue-styleguidist/vue-styleguidist/compare/v3.26.2...v4.0.0-beta.11) (2019-10-30)



# [4.0.0-beta.10](https://github.com/vue-styleguidist/vue-styleguidist/compare/v4.0.0-beta.9...v4.0.0-beta.10) (2019-10-30)



# [4.0.0-beta.9](https://github.com/vue-styleguidist/vue-styleguidist/compare/v4.0.0-beta.8...v4.0.0-beta.9) (2019-10-30)


### Features

* add progress bar while compiling ([f16b901](https://github.com/vue-styleguidist/vue-styleguidist/commit/f16b901))



# [4.0.0-beta.8](https://github.com/vue-styleguidist/vue-styleguidist/compare/v4.0.0-beta.7...v4.0.0-beta.8) (2019-10-28)


### Features

* emit types for vue-styleguidist ([f0af958](https://github.com/vue-styleguidist/vue-styleguidist/commit/f0af958))



# [4.0.0-beta.7](https://github.com/vue-styleguidist/vue-styleguidist/compare/v3.26.0...v4.0.0-beta.7) (2019-10-25)



# [4.0.0-beta.6](https://github.com/vue-styleguidist/vue-styleguidist/compare/v4.0.0-beta.5...v4.0.0-beta.6) (2019-10-24)



# [4.0.0-beta.5](https://github.com/vue-styleguidist/vue-styleguidist/compare/v4.0.0-beta.4...v4.0.0-beta.5) (2019-10-24)



# [4.0.0-beta.4](https://github.com/vue-styleguidist/vue-styleguidist/compare/v4.0.0-beta.3...v4.0.0-beta.4) (2019-10-24)


### Bug Fixes

* publish templates with vsg ([f8df33f](https://github.com/vue-styleguidist/vue-styleguidist/commit/f8df33f))



# [4.0.0-beta.3](https://github.com/vue-styleguidist/vue-styleguidist/compare/v3.25.1-beta.1...v4.0.0-beta.3) (2019-10-24)



## [3.25.1-beta.1](https://github.com/vue-styleguidist/vue-styleguidist/compare/v3.25.1-beta.0...v3.25.1-beta.1) (2019-10-23)


### Bug Fixes

* passing a webpackConfig should prioitize ([683f3dc](https://github.com/vue-styleguidist/vue-styleguidist/commit/683f3dc))
* split compiler & utils - efficient code split ([9ef9d06](https://github.com/vue-styleguidist/vue-styleguidist/commit/9ef9d06))
* **docgen:** make docgen output arrays only ([d456c6c](https://github.com/vue-styleguidist/vue-styleguidist/commit/d456c6c))


### Code Refactoring

* **docgen:** make function docgen.parse async ([e17680b](https://github.com/vue-styleguidist/vue-styleguidist/commit/e17680b))


### Features

* change defaults for codeSplit & simpleEditor ([810bf1c](https://github.com/vue-styleguidist/vue-styleguidist/commit/810bf1c))


### BREAKING CHANGES

* **docgen:** props, events, methods and slots are now all arrays

Co-authored-by: Sébastien D. <demsking@gmail.com>
* **docgen:** docgen becomes async, so do all of the handlers
* change defaults for `simpleEditor` mean that `editorConfig` will not work without `simpleEditor: false`
* compiler now exports compiler function as default
* isCodeVueSfc, styleScoper and adaptCreateElement
 are now their own package



## [3.25.1-beta.0](https://github.com/vue-styleguidist/vue-styleguidist/compare/v3.25.0...v3.25.1-beta.0) (2019-10-23)





# [4.0.0-beta.10](https://github.com/vue-styleguidist/vue-styleguidist/compare/v4.0.0-beta.9...v4.0.0-beta.10) (2019-10-30)

**Note:** Version bump only for package vue-styleguidist





# [4.0.0-beta.9](https://github.com/vue-styleguidist/vue-styleguidist/compare/v4.0.0-beta.8...v4.0.0-beta.9) (2019-10-30)


### Features

* add progress bar while compiling ([f16b901](https://github.com/vue-styleguidist/vue-styleguidist/commit/f16b901))





# [4.0.0-beta.8](https://github.com/vue-styleguidist/vue-styleguidist/compare/v4.0.0-beta.7...v4.0.0-beta.8) (2019-10-28)


### Features

* emit types for vue-styleguidist ([f0af958](https://github.com/vue-styleguidist/vue-styleguidist/commit/f0af958))





# [4.0.0-beta.7](https://github.com/vue-styleguidist/vue-styleguidist/compare/v4.0.0-beta.6...v4.0.0-beta.7) (2019-10-25)

**Note:** Version bump only for package vue-styleguidist





# [4.0.0-beta.6](https://github.com/vue-styleguidist/vue-styleguidist/compare/v4.0.0-beta.5...v4.0.0-beta.6) (2019-10-24)

**Note:** Version bump only for package vue-styleguidist





# [4.0.0-beta.5](https://github.com/vue-styleguidist/vue-styleguidist/compare/v4.0.0-beta.4...v4.0.0-beta.5) (2019-10-24)

**Note:** Version bump only for package vue-styleguidist





# [4.0.0-beta.4](https://github.com/vue-styleguidist/vue-styleguidist/compare/v4.0.0-beta.3...v4.0.0-beta.4) (2019-10-24)
## [3.26.2](https://github.com/vue-styleguidist/vue-styleguidist/compare/v3.26.1...v3.26.2) (2019-10-30)


### Bug Fixes

* publish templates with vsg ([f8df33f](https://github.com/vue-styleguidist/vue-styleguidist/commit/f8df33f))





# [4.0.0-beta.3](https://github.com/vue-styleguidist/vue-styleguidist/compare/v3.25.1-beta.1...v4.0.0-beta.3) (2019-10-24)

**Note:** Version bump only for package vue-styleguidist





# [4.0.0-beta.1](https://github.com/vue-styleguidist/vue-styleguidist/compare/v3.25.1-beta.1...v4.0.0-beta.1) (2019-10-23)

**Note:** Version bump only for package vue-styleguidist





## [3.25.1-beta.1](https://github.com/vue-styleguidist/vue-styleguidist/compare/v3.25.1-beta.0...v3.25.1-beta.1) (2019-10-23)


### Bug Fixes

* passing a webpackConfig should prioitize ([683f3dc](https://github.com/vue-styleguidist/vue-styleguidist/commit/683f3dc))
* split compiler & utils - efficient code split ([9ef9d06](https://github.com/vue-styleguidist/vue-styleguidist/commit/9ef9d06))
* **docgen:** make docgen output arrays only ([d456c6c](https://github.com/vue-styleguidist/vue-styleguidist/commit/d456c6c))


### Code Refactoring

* **docgen:** make function docgen.parse async ([e17680b](https://github.com/vue-styleguidist/vue-styleguidist/commit/e17680b))


### Features

* change defaults for codeSplit & simpleEditor ([810bf1c](https://github.com/vue-styleguidist/vue-styleguidist/commit/810bf1c))


### BREAKING CHANGES

* **docgen:** props, events, methods and slots are now all arrays

Co-authored-by: Sébastien D. <demsking@gmail.com>
* **docgen:** docgen becomes async, so do all of the handlers
* change defaults for `simpleEditor` mean that `editorConfig` will not work without `simpleEditor: false`
* compiler now exports compiler function as default
* isCodeVueSfc, styleScoper and adaptCreateElement
 are now their own package
* make sure defaults are there for the plugin ([00a05ac](https://github.com/vue-styleguidist/vue-styleguidist/commit/00a05ac)), closes [#615](https://github.com/vue-styleguidist/vue-styleguidist/issues/615)





## [3.25.1-beta.0](https://github.com/vue-styleguidist/vue-styleguidist/compare/v3.25.0...v3.25.1-beta.0) (2019-10-23)
# [3.26.0](https://github.com/vue-styleguidist/vue-styleguidist/compare/v3.25.0...v3.26.0) (2019-10-25)


### Bug Fixes

* tag class for JsDoc tag values ([38fdd46](https://github.com/vue-styleguidist/vue-styleguidist/commit/38fdd46))


### Features

* readable css class for JsDoc results ([a56f341](https://github.com/vue-styleguidist/vue-styleguidist/commit/a56f341)), closes [#602](https://github.com/vue-styleguidist/vue-styleguidist/issues/602)





# [3.25.0](https://github.com/vue-styleguidist/vue-styleguidist/compare/v3.24.2...v3.25.0) (2019-10-15)

**Note:** Version bump only for package vue-styleguidist





## [3.24.2](https://github.com/vue-styleguidist/vue-styleguidist/compare/v3.24.1...v3.24.2) (2019-09-26)

**Note:** Version bump only for package vue-styleguidist





## [3.24.1](https://github.com/vue-styleguidist/vue-styleguidist/compare/v3.24.0...v3.24.1) (2019-09-26)

**Note:** Version bump only for package vue-styleguidist





## [3.23.2](https://github.com/vue-styleguidist/vue-styleguidist/compare/v3.23.1...v3.23.2) (2019-09-24)


### Bug Fixes

* add .vue to extension array in webpack config ([65da41b](https://github.com/vue-styleguidist/vue-styleguidist/commit/65da41b))





## [3.23.1](https://github.com/vue-styleguidist/vue-styleguidist/compare/v3.23.0...v3.23.1) (2019-09-20)

**Note:** Version bump only for package vue-styleguidist





# [3.23.0](https://github.com/vue-styleguidist/vue-styleguidist/compare/v3.22.3...v3.23.0) (2019-09-19)

**Note:** Version bump only for package vue-styleguidist





## [3.22.3](https://github.com/vue-styleguidist/vue-styleguidist/compare/v3.22.2...v3.22.3) (2019-09-12)

**Note:** Version bump only for package vue-styleguidist





## [3.22.2](https://github.com/vue-styleguidist/vue-styleguidist/compare/v3.22.1...v3.22.2) (2019-08-24)


### Bug Fixes

* Fix bad links to docs ([a5a8978](https://github.com/vue-styleguidist/vue-styleguidist/commit/a5a8978))





## [3.22.1](https://github.com/vue-styleguidist/vue-styleguidist/compare/v3.22.0...v3.22.1) (2019-08-19)

**Note:** Version bump only for package vue-styleguidist





# [3.22.0](https://github.com/vue-styleguidist/vue-styleguidist/compare/v3.21.0...v3.22.0) (2019-08-19)


### Bug Fixes

* update react styleguidist to fix menu ([00ec66a](https://github.com/vue-styleguidist/vue-styleguidist/commit/00ec66a)), closes [#561](https://github.com/vue-styleguidist/vue-styleguidist/issues/561)
* update react styleguidist to fix menu (2) ([dba9fbf](https://github.com/vue-styleguidist/vue-styleguidist/commit/dba9fbf)), closes [#561](https://github.com/vue-styleguidist/vue-styleguidist/issues/561)





# [3.21.0](https://github.com/vue-styleguidist/vue-styleguidist/compare/v3.20.5...v3.21.0) (2019-08-17)

**Note:** Version bump only for package vue-styleguidist





# [3.20.0](https://github.com/vue-styleguidist/vue-styleguidist/compare/v3.19.5...v3.20.0) (2019-08-10)


### Bug Fixes

* update react-styleguidist ([e820c12](https://github.com/vue-styleguidist/vue-styleguidist/commit/e820c12)), closes [#492](https://github.com/vue-styleguidist/vue-styleguidist/issues/492) [#199](https://github.com/vue-styleguidist/vue-styleguidist/issues/199)





## [3.19.5](https://github.com/vue-styleguidist/vue-styleguidist/compare/v3.19.4...v3.19.5) (2019-08-07)


### Bug Fixes

* EditorWithToolbar naming case ([595a077](https://github.com/vue-styleguidist/vue-styleguidist/commit/595a077))


### Performance Improvements

* only precompile example for prod ([b7aeb58](https://github.com/vue-styleguidist/vue-styleguidist/commit/b7aeb58))





## [3.19.4](https://github.com/vue-styleguidist/vue-styleguidist/compare/v3.19.3...v3.19.4) (2019-08-06)


### Bug Fixes

* webpack dependency for yarn + storybook ([e4c5d2e](https://github.com/vue-styleguidist/vue-styleguidist/commit/e4c5d2e))





## [3.19.3](https://github.com/vue-styleguidist/vue-styleguidist/compare/v3.19.2...v3.19.3) (2019-08-06)


### Bug Fixes

* add webpack peerDependency ([16b1fa7](https://github.com/vue-styleguidist/vue-styleguidist/commit/16b1fa7))


### Performance Improvements

* remove multiple dependencies ([0927e85](https://github.com/vue-styleguidist/vue-styleguidist/commit/0927e85))





## [3.19.2](https://github.com/vue-styleguidist/vue-styleguidist/compare/v3.19.1...v3.19.2) (2019-08-05)


### Bug Fixes

* avoid dependency to webpack ([63ee996](https://github.com/vue-styleguidist/vue-styleguidist/commit/63ee996))





## [3.19.1](https://github.com/vue-styleguidist/vue-styleguidist/compare/v3.19.0...v3.19.1) (2019-08-04)


### Bug Fixes

* <docs src=> should not look at script tag ([2cef0d4](https://github.com/vue-styleguidist/vue-styleguidist/commit/2cef0d4))
* avoid hmr loop in plugin usage ([c6e4adf](https://github.com/vue-styleguidist/vue-styleguidist/commit/c6e4adf))





# [3.19.0](https://github.com/vue-styleguidist/vue-styleguidist/compare/v3.18.1...v3.19.0) (2019-08-02)


### Bug Fixes

* default example only appear when no doc ([b3b4156](https://github.com/vue-styleguidist/vue-styleguidist/commit/b3b4156))
* webpackConfig has priority on publicPath ([a06c1c6](https://github.com/vue-styleguidist/vue-styleguidist/commit/a06c1c6)), closes [#529](https://github.com/vue-styleguidist/vue-styleguidist/issues/529)
* wrong propTypes for playgroundAsync ([3fffa13](https://github.com/vue-styleguidist/vue-styleguidist/commit/3fffa13))


### Features

* allow import syntax ([5c61678](https://github.com/vue-styleguidist/vue-styleguidist/commit/5c61678)), closes [#104](https://github.com/vue-styleguidist/vue-styleguidist/issues/104)





## [3.18.1](https://github.com/vue-styleguidist/vue-styleguidist/compare/v3.18.0...v3.18.1) (2019-07-30)


### Bug Fixes

* editor should update when changing page ([35d0c3f](https://github.com/vue-styleguidist/vue-styleguidist/commit/35d0c3f))





# [3.18.0](https://github.com/vue-styleguidist/vue-styleguidist/compare/v3.17.2...v3.18.0) (2019-07-28)


### Bug Fixes

* async conlict with routing ([75424f7](https://github.com/vue-styleguidist/vue-styleguidist/commit/75424f7))
* better PropTypes for PlaygroundAsync ([3b60e3e](https://github.com/vue-styleguidist/vue-styleguidist/commit/3b60e3e))


### Features

* add copyCodeButton option ([90767af](https://github.com/vue-styleguidist/vue-styleguidist/commit/90767af))





## [3.17.2](https://github.com/vue-styleguidist/vue-styleguidist/compare/v3.17.1...v3.17.2) (2019-07-26)


### Bug Fixes

* make codeSplit comptible with jsxInExamples ([83c0bf6](https://github.com/vue-styleguidist/vue-styleguidist/commit/83c0bf6))





## [3.17.1](https://github.com/vue-styleguidist/vue-styleguidist/compare/v3.17.0...v3.17.1) (2019-07-26)


### Bug Fixes

* precompile examples when codeSplit ([d75f3f4](https://github.com/vue-styleguidist/vue-styleguidist/commit/d75f3f4))
* remove the propTypes error in codeSplit ([ea53a14](https://github.com/vue-styleguidist/vue-styleguidist/commit/ea53a14))





# [3.17.0](https://github.com/vue-styleguidist/vue-styleguidist/compare/v3.16.3...v3.17.0) (2019-07-23)


### Bug Fixes

* make sure code split works with prism ([51e7660](https://github.com/vue-styleguidist/vue-styleguidist/commit/51e7660))


### Features

* add codeSplit option for compiler ([286e2ee](https://github.com/vue-styleguidist/vue-styleguidist/commit/286e2ee))
* when codeSplit lazy load codemirror editor ([6f83989](https://github.com/vue-styleguidist/vue-styleguidist/commit/6f83989))





## [3.16.3](https://github.com/vue-styleguidist/vue-styleguidist/compare/v3.16.2...v3.16.3) (2019-07-19)


### Bug Fixes

*  evaluation was failing ([467949f](https://github.com/vue-styleguidist/vue-styleguidist/commit/467949f))





## [3.16.2](https://github.com/vue-styleguidist/vue-styleguidist/compare/v3.16.1...v3.16.2) (2019-07-17)


### Bug Fixes

* use the spread in styleguidist ([fd464a8](https://github.com/vue-styleguidist/vue-styleguidist/commit/fd464a8))


### Performance Improvements

* avoid loading pragma without jsx ([5b5012b](https://github.com/vue-styleguidist/vue-styleguidist/commit/5b5012b))





## [3.16.1](https://github.com/vue-styleguidist/vue-styleguidist/compare/v3.16.0...v3.16.1) (2019-07-16)

**Note:** Version bump only for package vue-styleguidist





# [3.16.0](https://github.com/vue-styleguidist/vue-styleguidist/compare/v3.15.4...v3.16.0) (2019-07-15)


### Bug Fixes

* bump clipboard-copy version ([b3c86d9](https://github.com/vue-styleguidist/vue-styleguidist/commit/b3c86d9)), closes [#500](https://github.com/vue-styleguidist/vue-styleguidist/issues/500)
* rename createElement ([429dd96](https://github.com/vue-styleguidist/vue-styleguidist/commit/429dd96))


### Features

* use styleguidePublicPath in server ([bd5e3ec](https://github.com/vue-styleguidist/vue-styleguidist/commit/bd5e3ec))
* use the JSX capabilities of compiler ([a6db6cb](https://github.com/vue-styleguidist/vue-styleguidist/commit/a6db6cb))





## [3.15.4](https://github.com/vue-styleguidist/vue-styleguidist/compare/v3.15.3...v3.15.4) (2019-07-07)

**Note:** Version bump only for package vue-styleguidist





## [3.15.3](https://github.com/vue-styleguidist/vue-styleguidist/compare/v3.15.2...v3.15.3) (2019-07-02)


### Bug Fixes

* **codemirror:** allow for mulitple words in cm themes ([6168883](https://github.com/vue-styleguidist/vue-styleguidist/commit/6168883)), closes [#480](https://github.com/vue-styleguidist/vue-styleguidist/issues/480)





## [3.15.2](https://github.com/vue-styleguidist/vue-styleguidist/compare/v3.15.1...v3.15.2) (2019-07-02)


### Bug Fixes

* render default value empty string ([f41869d](https://github.com/vue-styleguidist/vue-styleguidist/commit/f41869d))





## [3.15.1](https://github.com/vue-styleguidist/vue-styleguidist/compare/v3.15.0...v3.15.1) (2019-06-27)

**Note:** Version bump only for package vue-styleguidist





# [3.15.0](https://github.com/vue-styleguidist/vue-styleguidist/compare/v3.14.5...v3.15.0) (2019-06-19)

**Note:** Version bump only for package vue-styleguidist





## [3.14.5](https://github.com/vue-styleguidist/vue-styleguidist/compare/v3.14.4...v3.14.5) (2019-06-14)

**Note:** Version bump only for package vue-styleguidist





## [3.14.4](https://github.com/vue-styleguidist/vue-styleguidist/compare/v3.14.3...v3.14.4) (2019-06-14)


### Bug Fixes

* update dependencies to re-enable HMR ([860e3bc](https://github.com/vue-styleguidist/vue-styleguidist/commit/860e3bc))





## [3.14.3](https://github.com/vue-styleguidist/vue-styleguidist/compare/v3.14.2...v3.14.3) (2019-06-10)


### Bug Fixes

* reorder aliases to allow Styleguide overrides ([9195772](https://github.com/vue-styleguidist/vue-styleguidist/commit/9195772))





## [3.14.2](https://github.com/vue-styleguidist/vue-styleguidist/compare/v3.14.1...v3.14.2) (2019-06-06)

**Note:** Version bump only for package vue-styleguidist





## [3.14.1](https://github.com/vue-styleguidist/vue-styleguidist/compare/v3.14.0...v3.14.1) (2019-06-05)

**Note:** Version bump only for package vue-styleguidist





# [3.14.0](https://github.com/vue-styleguidist/vue-styleguidist/compare/v3.13.10...v3.14.0) (2019-06-05)


### Features

* add minimize options ([93ad5d3](https://github.com/vue-styleguidist/vue-styleguidist/commit/93ad5d3))





## [3.13.10](https://github.com/vue-styleguidist/vue-styleguidist/compare/v3.13.9...v3.13.10) (2019-06-04)


### Bug Fixes

* re-use the react hmr plugin ([2dfc5ad](https://github.com/vue-styleguidist/vue-styleguidist/commit/2dfc5ad))





## [3.13.9](https://github.com/vue-styleguidist/vue-styleguidist/compare/v3.13.8...v3.13.9) (2019-05-29)


### Bug Fixes

* **preview:** fix style scope mismatch ([830abf8](https://github.com/vue-styleguidist/vue-styleguidist/commit/830abf8)), closes [#437](https://github.com/vue-styleguidist/vue-styleguidist/issues/437)
* **preview:** gracefully fail when Vue breaks ([1152600](https://github.com/vue-styleguidist/vue-styleguidist/commit/1152600)), closes [#435](https://github.com/vue-styleguidist/vue-styleguidist/issues/435)





## [3.13.8](https://github.com/vue-styleguidist/vue-styleguidist/compare/v3.13.7...v3.13.8) (2019-05-29)


### Bug Fixes

* **editor:** make sure when url changes editor is repainted ([2dcbaac](https://github.com/vue-styleguidist/vue-styleguidist/commit/2dcbaac)), closes [#404](https://github.com/vue-styleguidist/vue-styleguidist/issues/404)





## [3.13.7](https://github.com/vue-styleguidist/vue-styleguidist/compare/v3.13.6...v3.13.7) (2019-05-24)


### Bug Fixes

* make hidden components work again ([4898fee](https://github.com/vue-styleguidist/vue-styleguidist/commit/4898fee))





## [3.13.6](https://github.com/vue-styleguidist/vue-styleguidist/compare/v3.13.5...v3.13.6) (2019-05-23)


### Bug Fixes

* **core:** example loader needs to require only on the script ([0c045df](https://github.com/vue-styleguidist/vue-styleguidist/commit/0c045df)), closes [#421](https://github.com/vue-styleguidist/vue-styleguidist/issues/421)





## [3.13.5](https://github.com/vue-styleguidist/vue-styleguidist/compare/v3.13.4...v3.13.5) (2019-05-22)


### Bug Fixes

* **core:** remove self require in readme ([b6408af](https://github.com/vue-styleguidist/vue-styleguidist/commit/b6408af)), closes [#407](https://github.com/vue-styleguidist/vue-styleguidist/issues/407)





## [3.13.4](https://github.com/vue-styleguidist/vue-styleguidist/compare/v3.13.3...v3.13.4) (2019-05-15)

**Note:** Version bump only for package vue-styleguidist





## [3.13.3](https://github.com/vue-styleguidist/vue-styleguidist/compare/v3.13.2...v3.13.3) (2019-05-14)


### Bug Fixes

* register all cmpnts ins/f only first section ([4ae5390](https://github.com/vue-styleguidist/vue-styleguidist/commit/4ae5390)), closes [#405](https://github.com/vue-styleguidist/vue-styleguidist/issues/405)





## [3.13.2](https://github.com/vue-styleguidist/vue-styleguidist/compare/v3.13.1...v3.13.2) (2019-05-13)


### Bug Fixes

* **core:** fix Preview.js with pure md files ([d52feea](https://github.com/vue-styleguidist/vue-styleguidist/commit/d52feea)), closes [#411](https://github.com/vue-styleguidist/vue-styleguidist/issues/411)





## [3.13.1](https://github.com/vue-styleguidist/vue-styleguidist/compare/v3.13.0...v3.13.1) (2019-04-29)


### Bug Fixes

* cleanComponentName peace with babel 7 ([bd8a085](https://github.com/vue-styleguidist/vue-styleguidist/commit/bd8a085))
* transform import was not working properly ([a6df22b](https://github.com/vue-styleguidist/vue-styleguidist/commit/a6df22b))





# [3.13.0](https://github.com/vue-styleguidist/vue-styleguidist/compare/v3.12.0...v3.13.0) (2019-04-28)


### Features

* allow components to be registered only locally ([#398](https://github.com/vue-styleguidist/vue-styleguidist/issues/398)) ([1dd2f1d](https://github.com/vue-styleguidist/vue-styleguidist/commit/1dd2f1d)), closes [#2](https://github.com/vue-styleguidist/vue-styleguidist/issues/2)





# [3.12.0](https://github.com/vue-styleguidist/vue-styleguidist/compare/v3.11.7...v3.12.0) (2019-04-25)


### Features

* **main:** add jsxInComponents option ([27b4257](https://github.com/vue-styleguidist/vue-styleguidist/commit/27b4257))





## [3.11.7](https://github.com/vue-styleguidist/vue-styleguidist/compare/v3.11.6...v3.11.7) (2019-04-23)


### Bug Fixes

* keep dashes in component names ([3ec75ed](https://github.com/vue-styleguidist/vue-styleguidist/commit/3ec75ed)), closes [#391](https://github.com/vue-styleguidist/vue-styleguidist/issues/391)
* make sure we detect all variables ([118f1a8](https://github.com/vue-styleguidist/vue-styleguidist/commit/118f1a8))
* parse es6 imports with vsg format ([8f5ff19](https://github.com/vue-styleguidist/vue-styleguidist/commit/8f5ff19))
* remove getVars blocking imports ([1066123](https://github.com/vue-styleguidist/vue-styleguidist/commit/1066123))





## [3.11.6](https://github.com/vue-styleguidist/vue-styleguidist/compare/v3.11.5...v3.11.6) (2019-04-23)


### Bug Fixes

* ignored errors ([#177](https://github.com/vue-styleguidist/vue-styleguidist/issues/177)) ([#316](https://github.com/vue-styleguidist/vue-styleguidist/issues/316)) ([298f462](https://github.com/vue-styleguidist/vue-styleguidist/commit/298f462))





## [3.11.5](https://github.com/vue-styleguidist/vue-styleguidist/compare/v3.11.4...v3.11.5) (2019-04-20)

**Note:** Version bump only for package vue-styleguidist





## [3.11.4](https://github.com/vue-styleguidist/vue-styleguidist/compare/v3.11.3...v3.11.4) (2019-04-03)

**Note:** Version bump only for package vue-styleguidist





## [3.11.3](https://github.com/vue-styleguidist/vue-styleguidist/compare/v3.11.2...v3.11.3) (2019-04-01)


### Bug Fixes

* **options:** allow two words in displayName ([7b72603](https://github.com/vue-styleguidist/vue-styleguidist/commit/7b72603))
* **plugin:** issue with babel ([afbf21a](https://github.com/vue-styleguidist/vue-styleguidist/commit/afbf21a))
* **safety:** update css-loader ([0b074b8](https://github.com/vue-styleguidist/vue-styleguidist/commit/0b074b8))





## [3.11.2](https://github.com/vue-styleguidist/vue-styleguidist/compare/v3.11.1...v3.11.2) (2019-03-28)

**Note:** Version bump only for package vue-styleguidist





## [3.11.1](https://github.com/vue-styleguidist/vue-styleguidist/compare/v3.11.0...v3.11.1) (2019-03-28)

**Note:** Version bump only for package vue-styleguidist





# [3.11.0](https://github.com/vue-styleguidist/vue-styleguidist/compare/v3.10.2...v3.11.0) (2019-03-26)


### Features

* **core:** update react styleguidist to 9.0.4 ([#344](https://github.com/vue-styleguidist/vue-styleguidist/issues/344)) ([1ec6e64](https://github.com/vue-styleguidist/vue-styleguidist/commit/1ec6e64))





## [3.10.2](https://github.com/vue-styleguidist/vue-styleguidist/compare/v3.10.1...v3.10.2) (2019-03-22)


### Performance Improvements

* **esprima:** get rid of esprima heavy loader ([#347](https://github.com/vue-styleguidist/vue-styleguidist/issues/347)) ([552ba14](https://github.com/vue-styleguidist/vue-styleguidist/commit/552ba14))
