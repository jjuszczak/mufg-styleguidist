/* eslint-disable import/first */
import 'react-styleguidist/lib/client/polyfills'
import 'react-styleguidist/lib/client/styles'
import ReactDOM from 'react-dom'
import { getParameterByName, hasInHash } from 'react-styleguidist/lib/client/utils/handleHash'
import renderStyleguide from './utils/renderStyleguide'

// Examples code revision to rerender only code examples (not the whole page) when code changes
// eslint-disable-next-line no-unused-vars
let codeRevision = 0

// eslint-disable-next-line
// @ts-ignore
// eslint-disable-next-line @typescript-eslint/naming-convention
const __qiankun__ = window.__POWERED_BY_QIANKUN__



// Scrolls to origin when current window location hash points to an isolated view.
const scrollToOrigin = () => {
	const hash = window.location.hash
	if (hasInHash(hash, '#/') || hasInHash(hash, '#!/')) {
		// Extracts the id param of hash
		const idHashParam = getParameterByName(hash, 'id')

		// For default scroll scrollTop is the page top
		let scrollTop = 0

		if (idHashParam) {
			// Searches the node with the same id, takes his offsetTop
			// And with offsetTop, tries to scroll to node
			const idElement = document.getElementById(idHashParam)
			if (idElement && idElement.offsetTop) {
				scrollTop = idElement.offsetTop
			}
		}
		window.scrollTo(0, scrollTop)
	}
}

const render = () => {
	// eslint-disable-next-line @typescript-eslint/no-var-requires,import/no-unresolved,import/extensions
	const styleguide = require('!!../loaders/styleguide-loader!./index.js')
	ReactDOM.render(
		renderStyleguide(styleguide, codeRevision),
		document.getElementById(styleguide.config.mountPointId)
	)
}

window.addEventListener('hashchange', render)
window.addEventListener('hashchange', scrollToOrigin)

if (module.hot) {
	module.hot.accept('!!../loaders/styleguide-loader!./index.js', () => {
		codeRevision += 1
		render()
	})
}

/**
 * The bootstrap will only be called once when the child application is initialized.
 * The next time the child application re-enters, the mount hook will be called directly, and bootstrap will not be triggered repeatedly.
 * Usually we can do some initialization of global variables here,
 * such as application-level caches that will not be destroyed during the unmount phase.
 */
 export async function bootstrap() {
  console.log('styleguide bootstrap');
}

/**
 * The mount method is called every time the application enters,
 * usually we trigger the application's rendering method here.
 */
export async function mount(props) {
	console.log('mount')
  render();
}

/**
 * Methods that are called each time the application is switched/unloaded,
 * usually in this case we uninstall the application instance of the subapplication.
 */
export async function unmount(props) {
	console.log('unmount')
	const styleguide = require('!!../loaders/styleguide-loader!./index.js');
  ReactDOM.unmountComponentAtNode(
    document.getElementById(styleguide.config.mountPointId)
  );
}

/**
 * Optional lifecycle，just available with loadMicroApp way
 */
export async function update(props) {
  console.log('update props', props);
}

if (!__qiankun__) {
	console.log('No __qiankun__ found. Rendering in standalone mode!')
	render()
}

export default {
	mount,
	unmount,
	update,
	bootstrap
}

